const webpack = require("webpack");
const childProcess = require("child_process");

const commitHash = childProcess
  .execSync("git rev-parse --short HEAD")
  .toString();

module.exports = {
  publicPath: "/",
  lintOnSave: false,
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        VERSION: JSON.stringify(require("./package.json").version),
        COMMIT_HASH: JSON.stringify(commitHash)
      })
    ],
    module: {
      rules: [
        {
          test: /\.md$/i,
          use: "raw-loader"
        }
      ]
    }
  },
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule.use("vue-svg-loader").loader("vue-svg-loader");
  },
  pwa: {
    name: "D&D Tools",
    themeColor: "#3273dc",
    iconPaths: {
      favicon32: "img/icons/logo-32x32.png",
      favicon16: "img/icons/logo-16x16.png",
      appleTouchIcon: "img/icons/logo-180x180.png",
      maskIcon: "img/icons/safari-pinned-tab.svg",
      msTileImage: "img/icons/logo.svg"
    },
    manifestOptions: {
      orientation: "any",
      icons: [
        {
          src: "./img/icons/logo-192x192.png",
          sizes: "192x192",
          type: "image/png"
        },
        {
          src: "./img/icons/logo-512x512.png",
          sizes: "512x512",
          type: "image/png"
        }
      ]
    },
    workboxOptions: {
      swDest: "sw.js",
      clientsClaim: true,
      skipWaiting: true
    }
  }
};
