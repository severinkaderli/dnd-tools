import originalMarked from "marked";

const marked = function (content) {
  if (content) {
    const renderer = new originalMarked.Renderer();
    renderer.image = function (href, title, text) {
      title = text;
      return `
          <figure>
            <img loading="lazy" src="${href}" alt="${text}" title="${title}"></figure>`;
    };

    return originalMarked(content, { renderer });
  }

  return "";
};

export default marked;
