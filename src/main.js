import Vue from "vue";
import {
  Table,
  Field,
  Switch,
  Input,
  Collapse,
  Icon,
  Tabs,
  Radio,
  Modal,
  Dropdown,
  Navbar,
  Select,
  Checkbox
} from "buefy";
import App from "./App.vue";
import router from "./router";
import "./registerServiceWorker";

Vue.config.productionTip = false;

Vue.use(Field);
Vue.use(Icon);
Vue.use(Input);
Vue.use(Collapse);
Vue.use(Switch);
Vue.use(Navbar);
Vue.use(Select);
Vue.use(Modal);
Vue.use(Dropdown);
Vue.use(Table);
Vue.use(Tabs);
Vue.use(Radio);
Vue.use(Checkbox);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
