_Beasthide:_ You gain an additional +1 bonus to your AC.   
_Longstride:_ Your movement ignores difficult terrain.   
_Razorclaw:_ Your attacks score a critical hit on a roll of 19 or 20.   
_Wildhunt:_ You can't be surprised while you are conscious.  