import manifestationMarkdown from "./manifestation.md";
import predatorformMarkdown from "./predatorform.md";

export default [
  {
    "name": "Actor",
    "prerequisite": "Charisma of 13 or higher",
    "asi": [
      "Charisma"
    ],
    "description": [
      "You gain proficiency in the Performance skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You can mimic the speech of another person or the sounds made by other creatures. You must have heard the person speaking, or heard the creature make the sound."
    ]
  },
    {
    "name": "Acrobat",
    "prerequisite": "Dexterity of 13 or higher",
    "asi": [
      "Dexterity"
    ],
    "description": [
      "You gain proficiency in the Acrobatics skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "Moving through difficult terrain doesn't cost you extra movement."
    ]
  },
  {
    "name": "Resilient",
    "prerequisite": "Wisdom of 17 or higher",
    "asi": [
      "Wisdom"
    ],
    "description": [
      "You can add your Wisdom modifier instead of another ability modifier whenever you make a saving throw.",
      "WIP"
    ]
  },
  {
    "name": "Beast Whisperer",
    "prerequisite": "Wisdom of 13 or higher",
    "asi": [
      "Wisdom"
    ],
    "description": [
      "You gain proficiency in the Animal Handling skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You can use a bonus action on your turn to command one friendly beast within 60 feet of you that can hear you and that isn’t currently following the command of someone else. You decide now what action the beast will take and where it will move during its next turn, or you issue a general command, such as to guard a particular area."
    ]
  },
  {
    "name": "Arcanist",
    "prerequisite": "Intelligence of 13 or higher",
    "asi": [
      "Intelligence"
    ],
    "description": [
      "You gain proficiency in the Arcana skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You can attune to one additional magic item."
    ]
  },
  {
    "name": "Athlete",
    "prerequisite": "Strength of 13 or higher",
    "asi": [
      "Strength"
    ],
    "description": [
      "You gain proficiency in the Athletics skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "Climbing and Swimming doesn’t cost you extra movement."
    ]
  },
  {
    "name": "Blade Mastery",
    "prerequisite": "Strength or Dexterity of 13 or higher",
    "asi": [
      "Dexterity",
      "Strength"
    ],
    "description": [
      "You gain a +2 bonus to attack and damage rolls you make with any melee weapon that deals slashing damage.",
      "When you make an opportunity attack with a melee weapon that deals slashing damage, you have advantage on the attack roll."
    ]
  },
  {
    "name": "Bountiful Luck",
    "prerequisite": "Halfling",
    "asi": "any",
    "description": [
      "Whenever an allied creature you can see within 30 feet of you rolls a 1 on a d20 roll, you can use your reaction to let the ally reroll the die. The ally must use the new roll.",
      "When a creature you can see hits you with an attack, you can use your reaction to force that creature to reroll. Once you use this ability, you can't use it again until finish a short or long rest."
    ]
  },
  {
    "name": "Child of Light",
    "prerequisite": "Aasimar",
    "asi": "any",
    "description": [
      "When you use your Healing Touch feature, double the amount of regained hit points.",
      "You learn the Light cantrip."
    ]
  },
  {
    "name": "Combat Reflexes",
    "prerequisite": "Dexterity of 17 or higher",
    "asi": [
      "Dexterity"
    ],
    "description": [
      "You gain a second use of your reaction each turn.",
      "You can't be surprised while you are conscious."
    ]
  },
  {
    "name": "Crossbow Expert",
    "prerequisite": "Dexterity of 13 or higher",
    "asi": [
      "Dexterity"
    ],
    "description": [
      "You ignore the loading quality of crossbows with which you are proficient.",
      "Being within 5 feet of a hostile creature doesn’t impose disadvantage on your ranged attack rolls."
    ]
  },
  {
    "name": "Deadly Duelist",
    "prerequisite": "Strength or Dexterity of 13 or higher",
    "asi": [
      "Dexterity",
      "Strength"
    ],
    "description": [
      "You gain a +2 bonus to attack and damage rolls you make with any melee weapon that deals piercing damage.",
      "Whenever you have advantage on an attack roll you make with a melee weapon that deals piercing damage, you score a crit if both of the two d20 rolls would hit."
    ]
  },
      {
    "name": "Demonic Heritage",
    "prerequisite": "Tiefling",
    "asi": "any",
    "description": [
      "You gain resistance to fire and necrotic damage.",
      "You gain a flying speed of 50 feet. To use this speed, you cannot be wearing medium or heavy armor."
    ]
  },
      {
    "name": "Diplomat",
    "prerequisite": "Charisma of 13 or higher",
    "asi": [
      "Dexterity"
    ],
    "description": [
      "You gain proficiency in the Persuasion skill. If you are already proficient in this skill, you add double your proficiency bonus to checks you make with it.",
      "If you spend 1 minute talking to a humanoid who is not hostile to you and who can understand what you say, you can attempt to charm it. It must succeed on a Wisdom saving throw against a Charisma (Persuasion) check made by you or it is charmed by you as long as it remains within 60 feet of you and for 1 minute thereafter. This is not a magical effect."
    ]
  },
  {
    "name": "Dwarven Resilience",
    "prerequisite": "Dwarf",
    "asi": "any",
    "description": [
      "You have immunity against the poisoned, paralyzed and stunned condition.",
      "At the start of your turn, if you have no more than half of your hit points left, you can spend one hit die to heal yourself. Roll the die, add your Constitution modifier, and regain a number of hit points equal to the total."
    ]
  },
  {
    "name": "Elven Training",
    "prerequisite": "Elf",
    "asi": "any",
    "description": [
      "When you are wielding a melee weapon in one hand, the weapon gains the light trait for your attacks made with it.",
      "Once per turn, you can ignore disadvantage on one attack roll you make using Dexterity."
    ]
  },
  {
    "name": "Empathic",
    "prerequisite": "Wisdom of 13 or higher",
    "asi": [
      "Wisdom"
    ],
    "description": [
      "You gain proficiency in the insight skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You can use your action to try to get uncanny insight about one humanoid you can see within 30 feet of you. It must succeed on a Charisma saving throw against a Wisdom (Insight) check made by you or you gain advantage on all ability checks against that humanoid for 1 minute."
    ]
  },
  {
    "name": "Fade Away",
    "prerequisite": "Gnome",
    "asi": "any",
    "description": [
      "Whenever you take damage, you can use your reaction to magically become invisible until the start of your next turn.",
      "You can cast the Pass Without Trace spell at it’s lowest level once, and you regain the ability to cast it this way when you finish a long rest."
    ]
  },
  {
    "name": "Feint",
    "prerequisite": "Charisma of 13 or higher",
    "asi": [
      "Charisma"
    ],
    "description": [
      "You gain proficiency in the Deception skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "As a bonus action on your turn, you can try to deceive one humanoid you can see within 30 feet of you that can see and hear you. It must succeed on a Wisdom saving throw against a Charisma (Deception) check made by you. If it fails, you have advantage on the next attack roll against that humanoid. If it succeeds, the target can’t be deceived by you in this way for the next 24 hours."
    ]
  },
  {
    "name": "Fell Handed",
    "prerequisite": "Strength or Dexterity of 13 or higher",
    "asi": [
      "Dexterity",
      "Strength"
    ],
    "description": [
      "You gain a +2 bonus to attack and damage rolls you make with any melee weapon that deals bludgeoning damage.",
      "Whenever you have advantage on an attack roll you make with a melee weapon that deals bludgeoning damage against a creature that is prone, you deal maximum damage with that attack."
    ]
  },
  {
    "name": "Great Weapon Master",
    "prerequisite": "Strength of 13 or higher",
    "asi": [
      "Strength"
    ],
    "description": [
      "Before you make an attack with a weapon that has the two-handed property, you can choose to take a -5 penalty to the attack roll. If the attack hits, you add +10 bonus to the attack’s damage roll.",
      "Whenever you have advantage on a melee attack roll you make with a weapon that has the two-handed property against a large or smaller creature and hit, you can knock the target prone if the lower of the two d20 rolls would also hit the target."
    ]
  },
  {
    "name": "Heavy Armor Master",
    "prerequisite": "proficiency with heavy armor",
    "asi": [
      "Strength"
    ],
    "description": [
      "You gain a +1 bonus to your AC when you are wearing heavy armor.",
      "Your speed is not reduced by wearing heavy armor."
    ]
  },
  {
    "name": "Historian",
    "prerequisite": "Intelligence of 13 or higher",
    "asi": [
      "Intelligence"
    ],
    "description": [
      "You gain proficiency in the History skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "Whenever you could make a Wisdom (Medicine) or Intelligence (Religion) check, you can instead roll an Intelligence (History) check."
    ]
  },
  {
    "name": "Inspiring Leader",
    "prerequisite": "Charisma 17 or higher",
    "asi": [
      "Charisma"
    ],
    "description": [
      "You gain immunity against being frightened.",
      "During a short or long rest, you can inspire your companions, shoring up their resolve to fight. Choose a number of friendly creatures that you can see equal to your proficiency bonus. Each creature gains temporary hit points equal to your level + your Charisma modifier."
    ]
  },
  {
    "name": "Investigator",
    "prerequisite": "Intelligence of 13 or higher",
    "asi": [
      "Intelligence"
    ],
    "description": [
      "You gain proficiency in the Investigation skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "As a bonus action, you can observe a humanoid and learn certain information about it. The DM tells you if the humanoid is your equal, superior, or inferior concerning its spellcasting capabilities, it’s AC or its current hit points (your choice)."
    ]
  },
  {
    "name": "Light Armor Master",
    "prerequisite": "proficiency with light armor",
    "asi": [
      "Constitution"
    ],
    "description": [
      "You gain a +1 bonus to your AC when wearing light armor.",
      "When wearing light armor, you can disguise it to look like normal clothing."
    ]
  },
    {
    "name": "Mage Hunter",
    "prerequisite": "Strength or Dexterity of 13 or higher",
    "asi": "any",
    "description": [
      "When a creature that you can see casts a spell, you can use your reaction to make an opportunity attack against that creature.",
      "When you damage a creature that is concentrating on a spell, that creature has disadvantage on the saving throw it makes to maintain its concentration."
    ]
  },
  {
    "name": "Manifestation",
    "prerequisite": "Genasi",
    "asi": "any",
    "description": [
      "The spell that you gain from your Call of the Elements feature can be cast twice before you need to finish a long rest to be able to cast it again.",
      "You gain resistance against the damage type associated with your elemental ancestry."
    ],
    markdown: manifestationMarkdown
  },
  {
    "name": "Medic",
    "prerequisite": "Wisdom of 13 or higher",
    "asi": [
      "Wisdom"
    ],
    "description": [
      "You gain proficiency in the Medicine skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "Whenever you finish a short rest or long rest, choose a number of friendly creatures that you can see equal to your proficiency bonus. Each of the chosen creatures regains an expended hit die."
    ]
  },
  {
    "name": "Medium Armor Master",
    "prerequisite": "proficiency with medium armor",
    "asi": [
      "Constitution"
    ],
    "description": [
      "You gain a +1 bonus to your AC when you are wearing medium armor.",
      "Wearing medium armor does not impose disadvantage on your Dexterity (Stealth) checks."
    ]
  },
  {
    "name": "Menacing",
    "prerequisite": "Charisma of 13 or higher",
    "asi": [
      "Charisma"
    ],
    "description": [
      "You gain proficiency in the Intimidation skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "As a bonus action, you can attempt to demoralize one creature you can see within 30 feet of you that can see and hear you. It must succeed on a Wisdom saving throw against a Charisma (Intimidation) check made by you or be frightened by you until the end of your next turn."
    ]
  },
  {
    "name": "Naturalist",
    "prerequisite": "Intelligence of 13 or higher",
    "asi": [
      "Intelligence"
    ],
    "description": [
      "You gain proficiency in the Nature skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "Whenever you could make a Wisdom (Medicine) or Wisdom (Survival) check, you can instead roll an Intelligence (Nature) check."
    ]
  },
  {
    "name": "Orcish Fury",
    "prerequisite": "Orc",
    "asi": "any",
    "description": [
      "Once per turn when you roll damage for a weapon attack, you can reroll the weapon’s damage dice and use either result.",
      "When you are reduced to 0 hit points or are hit by a crit, you can use your reaction to make one weapon attack."
    ]
  },
  {
    "name": "Perceptive",
    "prerequisite": "Wisdom of 13 or higher",
    "asi": [
      "Wisdom"
    ],
    "description": [
      "You gain proficiency in the Perception skill.  If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You have a +5 bonus to your passive Wisdom (Perception)."
    ]
  },
  {
    "name": "Persona",
    "prerequisite": "Changeling",
    "asi": "any",
    "description": [
      "When you assume the physical form of a humanoid, you also learn all basic information about this person.",
      "You gain immunity against divinitation magic that tries to see through your disguise."
    ]
  },
  {
    "name": "Polearm Master",
    "prerequisite": "Strength or Dexterity of 13 or higher",
    "asi": [
      "Dexterity",
      "Strength"
    ],
    "description": [
      "When you take the attack action and attack with a weapon that has the reach property, you can use a bonus action on the same turn to make an additional melee attack with the same weapon against a different target.",
      "While you are wielding a weapon with the reach property, other creatures provoke an opportunity attack from you when they enter your reach."
    ]
  },
  {
    "name": "Predator Form",
    "prerequisite": "Shifter",
    "asi": "any",
    "description": [
      "You can use your Shifting feature as a bonus action on your turn and you gain additonal benefits from each type of shifting:"
    ],
    markdown: predatorformMarkdown
  },
  {
    "name": "Prodigy",
    "prerequisite": "Human",
    "asi": "any",
    "description": [
      "You gain proficiency with one tool of your choice and fluency in one language of your choice.",
      "Choose one skill in which you have proficiency. Whenever you make a check with your chosen skill, you can treat a d20 roll of 9 or lower as a 10."
    ]
  },
  {
    "name": "Quick-Fingered",
    "prerequisite": "Dexterity of 13 or higher",
    "asi": [
      "Dexterity"
    ],
    "description": [
      "You gain proficiency in the Sleight of Hand skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You can take the Steal action as a bonus action on your turn."
    ]
  },
  {
    "name": "Quick-Witted",
    "prerequisite": "Intelligence of 17 or higher",
    "asi": "any",
    "description": [
      "Each time you would have to make a Dexterity saving throw, you can make an Intelligence saving throw instead.",
      "You can add your Intelligence modifier to your Initiative roll."
    ]
  },
  {
    "name": "Reactive Regeneration",
    "prerequisite": "Lizardfolk",
    "asi": "any",
    "description": [
      "Whenever you take damage, you can use your reaction to regain hit points equal to half your level. This healing cannot exceed the damage taken.",
      "Whenever you take a long rest, you regain additional hit die equal to your proficiency bonus."
    ]
  },
  {
    "name": "Sharpshooter",
    "prerequisite": "Strength or Dexterity of 13 or higher",
    "asi": [
      "Dexterity",
      "Strength"
    ],
    "description": [
      "Attacking at long range does not impose disadvantage on your ranged weapon attack rolls.",
      "Before you make an attack with a ranged weapon that you are proficient with, you can choose to take a -5 penalty to the attack roll. If the attack hits, you add +10 to the attack’s damage roll."
    ]
  },
  {
    "name": "Shield Master",
    "prerequisite": "proficiency with shields",
    "asi": "any",
    "description": [
      "While wielding a shield, you can take the Shove action as a bonus action.",
      "If you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage and are wielding a shield, you instead take no damage if you succeed on the saving throw, and only half damage if you fail."
    ]
  },
  {
    "name": "Siege Monster",
    "prerequisite": "Minotaur",
    "asi": "any",
    "description": [
      "You deal double damage to constructs, objects and buildings with your attacks, spells or abilites.",
      "When you use your Rush feature and you move at least 10 feet in a straight line immediately before making the attack with your horns, you gain a +5 bonus to the attack’s damage roll."
    ]
  },
  {
    "name": "Spell Sniper",
    "prerequisite": "Spellcasting",
    "asi": [
      "Intelligence",
      "Wisdom",
      "Charisma"
    ],
    "description": [
      "When you cast a spell that requires you to make an attack roll, the spell’s range is doubled.",
      "Once per turn when you roll damage for a ranged spell attack, you can reroll the spell’s damage dice and use either result."
    ]
  },
  {
    "name": "Stealthy",
    "prerequisite": "Dexterity of 13 or higher",
    "asi": [
      "Dexterity"
    ],
    "description": [
      "You gain proficiency in the Stealth skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You can take the Hide action as a bonus action on your turn."
    ]
  },
  {
    "name": "Stone's Defense",
    "prerequisite": "Goliath",
    "asi": "any",
    "description": [
      "Your hit point maximum increases by an amount equal to your level when you gain this feat. Whenever you gain a level thereafter, your hit point maximum increases by an additional 1 hit point.",
      "You gain resistance against bludgeoning, piercing and slashing damage from non magical weapons."
    ]
  },
  {
    "name": "Survivalist",
    "prerequisite": "Wisdom of 13 or higher",
    "asi": [
      "Wisdom"
    ],
    "description": [
      "You gain proficiency in the Survival skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "You have advantage on saving throws made to avoid or resist traps and natural hazards."
    ]
  },
  {
    "name": "Teaching of the Roc",
    "prerequisite": "Aarakocra",
    "asi": "any",
    "description": [
      "You do not provoke opportunity attacks when you fly out of an enemy's reach.",
      "During the first round of combat, you have advantage on your attack rolls."
    ]
  },
  {
    "name": "Theologian",
    "prerequisite": "Intelligence of 13 or higher",
    "asi": [
      "Intelligence"
    ],
    "description": [
      "You gain proficiency in the Religion skill. If you are already proficient in the skill, you add double your proficiency bonus to checks you make with it.",
      "Whenever you could make an Intelligence (History) or Charisma (Performance) check, you can instead roll an Intelligence (Religion) check."
    ]
  },
  {
    "name": "Titan's Grip",
    "prerequisite": "Strength of 17 or higher",
    "asi": [
      "Strength"
    ],
    "description": [
      "You can ignore the two-handed property of melee weapons that you are proficient with.",
      "If you attempt to grapple a creature, it has disadvantage on its checks to avoid getting grappled by you and freeing itself from your grapple."
    ]
  },
  {
    "name": "Tough",
    "prerequisite": "Constitution of 17 or higher",
    "asi": [
      "Constitution"
    ],
    "description": [
      "Your hit point maximum increases by an amount equal to your level when you gain this feat. Whenever you gain a level thereafter, your hit point maximum increases by an additional 1 hit points.",
      "Whenever you roll a hit die to regain hit points during a rest, you can reroll each roll lower than your proficiency bonus."
    ]
  },
  {
    "name": "Trueborn",
    "prerequisite": "Dragonborn",
    "asi": "any",
    "description": [
      "You can use your Breath Weapon feature twice before you need to finish a short or long rest to be able to use it again.",
      "Increase the size of the cone of your Breath Weapon feature to 30 feet."
    ]
  },
  {
    "name": "War Caster",
    "prerequisite": "Spellcasting",
    "asi": [
      "Intelligence",
      "Wisdom",
      "Charisma"
    ],
    "description": [
      "You have advantage on Constitution saving throws that you make to maintain your concentration on a spell when you take damage.",
      "When a hostile creature’s movement provokes an opportunity attack from you, you can use your reaction to cast a spell at the creature, rather than making an opportunity attack. The spell must have a casting time of 1 action or 1 bonus action."
    ]
  }
];
