| Ancestry | Resistance |
|----------|------------|
| Air      | Thunder    |
| Earth    | Acid       |
| Fire     | Fire       |
| Water    | Cold       |
