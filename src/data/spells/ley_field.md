| d6 | Effect |
|----|--------|
|1|Double the range of your spells and the area they affect|
|2|Targets of your spells have disadvantage on their saving throws against them|
|3|A creature that takes damage from your spells is restrained until the end of its next turn|
|4|Your concentration can’t be broken by damage|
|5|Treat your spells as if it they were cast with a spell slot one level higher|
|6|When you cast a spell, you regain 1d6 hit points per level of the spell|