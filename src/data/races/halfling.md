The comforts of home are the goals of most halflings' lives: a place to settle in peace and quiet, far from marauding monsters and clashing armies; a blazing fire and a generous meal; fine drink and fine conversation. Though some halflings live out their days in remote agricultural communities, others form nomadic bands that travel constantly, lured by the open road and the wide horizon to discover the wonders of new lands and peoples. But even these wanderers love peace, food, hearth, and home, though home might be a wagon jostling along an dirt road or a raft floating downriver.  
Halflings are an affable and cheerful people. They cherish the bonds of family and friendship as well as the comforts of hearth and home, harboring few dreams of gold or glory. Even adventurers among them usually venture into the world for reasons of community, friendship, wanderlust, or curiosity. They love discovering new things, even simple things, such as an exotic food or an unfamiliar style of clothing. Halflings are easily moved to pity and hate to see any living thing suffer. They are generous, happily sharing what they have even in lean times.

---

As a halfling, you gain the following traits:

**Ability Score Increase:** Dexterity and Constitution  

**Languages:** Common, and one language of your choice  

**Size:** Your size is small  

**Speed:** Your base walking speed is 25 feet   

**Brave:** You have immunity against being frightened. 
 
**Lucky:** When you roll a 1 on the d20 for an Attack roll, ability check, or saving throw, you can reroll the die and must use the new roll.   

---

## Gallery
<div class="gallery">

![Halfling 1](img/races/halfling_1.jpg)
![Halfling 2](img/races/halfling_2.png)
![Halfling 3](img/races/halfling_3.jpg)

</div>