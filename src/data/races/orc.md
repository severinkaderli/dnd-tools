Orcs are aggressive, callous, and domineering. Bullies by nature, they respect strength and power as the highest virtues. On an almost instinctive level, orcs believe they are entitled to anything they want unless someone stronger can stop them from seizing it. Surrounded at all times by bitter enemies, orcs cultivate an attitude of indifference to pain, vicious tempers, and a fierce willingness to commit unspeakable acts of vengeance against anyone who dares to defy them. Orcs admire strength above all things. Even members of enemy races can sometimes win an orc’s grudging respect, or at least tolerance, if they break his nose enough times.

---

As an orc, you gain the following traits:

**Ability Score Increase:** Strength and Charisma  

**Languages:** Common and Orc

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Menacing:** You gain proficiency in the Intimidation skill.  
 
**Relentless:** When you are reduced to 0 hit points but not killed outright, you can drop to 1 hit point instead. You can’t use this feature again until you finish a long rest.   

---

## Gallery
<div class="gallery">

![Orc 1](img/races/orc_1.jpg)
![Orc 2](img/races/orc_2.jpg)
![Orc 3](img/races/orc_3.jpg)

</div>