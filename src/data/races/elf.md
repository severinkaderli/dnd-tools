Elves are a magical people of otherworldly grace, living in the world but not entirely part of it. They live in places of ethereal beauty, in the midst of ancient forests or in silvery spires glittering with faerie light, where soft music drifts through the air and gentle fragrances waft on the breeze. Elves love nature and magic, art and artistry, music and poetry, and the good things of the world. Like the branches of a young tree, elves are flexible in the face of danger. They trust in diplomacy and compromise to resolve differences before they escalate to violence. They have been known to retreat from intrusions into their woodland homes, confident that they can simply wait the invaders out. But when the need arises, elves reveal a stern martial side, demonstrating skill with sword, bow, and strategy.

Elves take up adventuring out of wanderlust. Since they are so long-lived, they can enjoy centuries of exploration and discovery. They dislike the pace of human society, which is regimented from day to day but constantly changing over decades, so they find careers that let them travel freely and set their own pace. 

---

As an elf, you gain the following traits:

**Ability Score Increase:** Dexterity and Intelligence 

**Languages:** Common and Elven  

**Size:** Your size is medium  

**Speed:** Your base walking speed is 35 feet   

**Keen Senses:** You have proficiency in the Perception skill.  
 
**Trance:** Elves don't need to sleep and can't be put to sleep by magic. Instead, they meditate deeply, remaining semiconscious, for 4 hours a day. If you take a long rest, you finish the rest after only 4 hours.   

---

## Gallery
<div class="gallery">

![Elf 1](img/races/elf_1.png)
![Elf 2](img/races/elf_2.jpg)
![Elf 3](img/races/elf_3.jpg)

</div>
