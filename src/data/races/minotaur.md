Minotaurs are strong in body, dedication and courage. They love battle and are willng to fight for their various causes. They combine a burning fury with keen tactics that make them excellent commanders as well as valuable adventurers. Minotaurs tend to vent their rage through violence, but they aren't generally quick to anger. They are passionate, loving their friends and partners fiercely, and they laugh loud and long at good jokes.  
Minotaur legends describe a small pantheon of heroes who established the minotaurs' place in the world. Almost every minotaur claims to be a descendant from one of these heroes.

---

As a Minotaur, you gain the following traits:

**Ability Score Increase:** Strength and Wisdom  

**Language:** Common, and one language of your choice

**Size:** Your size is medium

**Speed:** Your base walking speed is 35 feet

**Horns:** You are proficient with your natural melee weapon, your horns. They deal 1d8 piercing damage on a hit.   
   
**Rush:** When you use the Dash action during your turn, you can make a melee attack with your horns as part of that same action. If you hit the creature, it must succeed on a Strength saving throw against an Athletics check made by you or be knocked prone.   

---

## Gallery
<div class="gallery">

![Minotaur 1](img/races/minotaur_1.jpg)
![Minotaur 2](img/races/minotaur_2.jpg)
![Minotaur 3](img/races/minotaur_3.jpg)

</div>