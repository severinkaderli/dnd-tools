A constant hum of busy activity pervades the warrens and neighborhoods where gnomes form their close-knit communities. Louder sounds punctuate the hum: a crunch of grinding gears here, a minor explosion there, a yelp of surprise or triumph, and especially bursts of laughter. Gnomes take delight in life, enjoying every moment of invention, exploration, investigation, creation, and play. As far as gnomes are concerned, being alive is a wonderful thing, and they squeeze every ounce of enjoyment out of their three to five centuries of life and they seem to worry that even with all that time, they can't get in enough of the things they want to do and see.  
Curious and impulsive, gnomes might take up adventuring as a way to see the world or for the love of exploring. Others take to adventuring as a quick, if dangerous, path to wealth. Regardless of what spurs them to adventure, gnomes who adopt this way of life eke as much enjoyment out of it as they do out of any other activity they undertake. 

---

As a gnome, you gain the following traits:

**Ability Score Increase:** Intelligence and Wisdom  

**Languages:** Common and Dwarven 

**Size:** Your size is small  

**Speed:** Your base walking speed is 25 feet   

**Cunning:** You have advantage on all Intelligence, Wisdom, and Charisma saving throws against spells and magical effects.  
 
**Lorekeeper:** You gain proficiency in the Arcana skill.   

---

## Gallery
<div class="gallery">

![Gnome 1](img/races/gnome_1.jpg)
![Gnome 2](img/races/gnome_2.jpg)
![Gnome 3](img/races/gnome_3.jpeg)

</div>