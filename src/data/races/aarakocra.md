Sequestered in high mountains atop tall trees, the aarakocra, sometimes called birdfolk, evoke fear and wonder. Aarakocra enjoy peace and solitude. Most of them have little interest in dealing with other peoples and less interest in spending time on the ground. For this reason, it takes exceptional circumstance for an aarakocra to seek the adventurer's life. If they do however, they are great scouts and explorers, nowhere more comfortable than the sky. They prove dynamic and acrobatic fliers, moving with remakable speed and grace. 

---

As an Aarakocra, you gain the following traits:

**Ability Score Increase:** Dexterity and Wisdom  

**Languages:** Common and Auran  

**Size:** Your size is medium  

**Speed:** Your base walking speed is 25 feet   

**Flight:** You have a flying speed of 50 feet. To use this speed, you cannot be wearing medium or heavy armor. 
 
**Talons:** Your unarmed attacks deal 1d6 slashing damage. You can use Dexterity instead of Strength for the attack and damage rolls of your unarmed strikes.

---

## Gallery
<div class="gallery">

![Aarakocra 1](img/races/aarakocra_1.jpg)
![Aarakocra 2](img/races/aarakocra_2.jpg)
![Aarakocra 3](img/races/aarakocra_3.png)

</div>

