Lizardfolk are proud and powerful reptilian predators that make their communal homes in scattered villages deep within swamps and marshes. Uninterested in colonization of the dry lands and content with the simple weapons and rituals that have served them well for millennia, lizardfolk are viewed by many other races as backwater savages, but within their isolated communities lizardfolk are actually a vibrant people filled with tradition and an oral history stretching back to before humans walked upright.  
Though generally neutral, lizardfolks’ standoffish demeanor, staunch rejection of civilization’s “gifts,” and legendary ferocity in battle cause them to be viewed negatively by most humanoids.

---

As a lizardfolk, you gain the following traits:

**Ability Score Increase:** Constitution and Intelligence  

**Languages:** Common, and one language of your choice  

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Amphibious:** You can live and breathe underwater. 
 
**Eternal Memory:** You gain proficiency in the History skill. Additionally, you are immune against memory altering spells and effects and you can always recall once known information.

---

## Gallery
<div class="gallery">

![Lizardfolk 1](img/races/lizardfolk_1.jpg)
![Lizardfolk 2](img/races/lizardfolk_2.jpg)
![Lizardfolk 3](img/races/lizardfolk_3.jpg)

</div>
