Humans are the youngest, most adaptable and most ambitious race. Perhaps it is because of their shorter lives that they strive to achieve as much as they can in the years they are given. Or maybe they feel they have something to prove to the elder races, and that's why they build their mighty empires on the foundation of conquest and trade. Whatever drives them, humans are the innovators, the achievers, and the pioneers of the worlds. They live fully in the present, making them well suited to the adventuring life. Idividually and as a group, humans are opportunists, striving to leave a lasting legacy.

---

As a human, you gain the following traits:

**Ability Score Increase:** Two ability scores of your choice  

**Languages:** Common, and one language of your choice  

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Adaptive:** You are immune against the negative effects of extreme temperature. 

**Focused Study:** Choose a skill that you are proficient with. You add double your proficiency bonus to checks you make with the chosen skill.  

---

## Gallery
<div class="gallery">

![Human 1](img/races/human_1.jpg)
![Human 2](img/races/human_2.jpg)
![Human 3](img/races/human_3.png)

</div>
