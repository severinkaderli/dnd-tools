At the highest mountain peaks, far above the slopes where trees grow and where the air is thin and the frigid winds howl, dwell the reclusive goliaths. Few folk can claim to have seen a goliath, and fewer still can claim friendship with them. Their bodies look as if they are carved from mountain stone and give them great physical power. Their spirits take after the wandering wind, making them nomads and adventurers. Their hearts are infused with the col regard of their giant ancestors, leaving each goliath with the responsibility to earn its place.  
In some ways, the goliath drive to outdo themselves feeds into the grim inevitability of their decline and death. A goliath would much rather die in battle, at the peak of strength and skill, than endure the slow decay of old age. Few folk have ever meet an elderly goliath, and even those goliaths who have left their people grapple with the urge to give up their lives as their physical skills decay.

---

As a goliath, you gain the following traits:

**Ability Score Increase:** Strength and Constitution  

**Languages:** Common and Giant 

**Size:** your size is medium  

**Speed:** your base walking speed is 30 feet   

**Natural Athlete:** You gain proficiency in the Athletic skill.  
 
**Perseverance:** You ignore the effects of the first level of exhaustion. However, when you gain a second level of exhaustion, it counts as the second level and you suffer the effects normally.   

---

## Gallery
<div class="gallery">

![Goliath 1](img/races/goliath_1.jpg)
![Goliath 2](img/races/goliath_2.jpg)
![Goliath 3](img/races/goliath_3.jpg)

</div>