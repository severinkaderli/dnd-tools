Wherever humans can be found, there are changelings; the question is whether their presence is known. A changeling can shift its face and form with a thought. Many changelings use this gift as grifters, spies, and others who wish to deceive. This leads many people to treat known changelings with fear and suspicion. Only a few are raised in stable communities where changelings are true to their nature and deal openly with the people around them. Most are orphans, raised by other races, who find their way in the world without ever knowing another like themselves.

In creating a changeling adventurer, consider the character's relationships with people around them. Does the character conceal their true changeling nature? Do they embrace it? Do they have connections to other changelings or are they alone and in search of companions?

---

As a Changeling, you gain the following traits:

**Ability Score Increase:** Dexterity and Charisma 

**Languages:** Common, and two languages of your choice  

**Size:** your size is medium  

**Speed:** your base walking speed is 30 feet   

**Duplicity:** You gain proficiency in the Deception skill. 
 
**Shapechanger:** During a short or long rest, you can polymorph into any humanoid of your size that you have seen in the last 24 hours, or back into your true form. However, your equipment does not change with you. If you die, you revert to your natural appearance.   

