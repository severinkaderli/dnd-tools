Those who think of other planes at all consider them remote, distant realms, but planar influence can be felt throughout the world. It sometimes manifests in beings who, through an accident of birth, carry the power of the planes in their blood. The genasi are one such people. The elemental blood flowing through their veins manifests differently in each genasi, often as magical power. As rare beings, genasi might go their entire lives without encountering another one of their kind. There are no great genasi cities or empires. Genasi seldom have communities of their own and typically adopt the cultures and societies into which they are born. Some genasi live as outcasts, driven into exile for their heritage and strange magic, or assuming leadership of savage humanoids and weird cults in untamed lands. Others gain positions of great influence, especially where elemental beings are revered. 

You have elemental ancestry. Choose one type of element from the table below. The chosen type, as shown in the table, determines the cantrip and spell gained by your Call of the Elements feature.

| Element | Cantrip | Spell |
|---------|---------|-------|
| Air | _[Gust](spells#gust)_ | _[Blur](spells#blur)_ |
| Earth | _[Mold Earth](spells#mold_earth)_ | _[Pass Without Trace](spells#pass_without_trace)_ |
| Fire | _[Controll Flames](spells#controll_flames)_ | _[Darkvision](spells#darkvision)_ |
| Water | _[Shape Water](spells#shape_water)_ | _[Misty Step](spells#misty_step)_ |

---

As a genasi, you gain the following traits:

**Ability Score Increase:** Constitution and Charisma  

**Languages:** Common and Primordial

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Affinity:** You gain advantage on Charisma checks when interacting with elementals.   
 
**Call of the Elements:** You know a cantrip and a spell associated with your elemental ancestry. You can cast the spell at it’s lowest level once with this trait, and you regain the ability to cast it this way when you finish a long rest.   

---

## Gallery
<div class="gallery">

![Genasi 1](img/races/genasi_1.jpg)
![Genasi 2](img/races/genasi_2.jpg)
![Genasi 3](img/races/genasi_3.jpg)

</div>
