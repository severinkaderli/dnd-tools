To be greeted with stares and whispers, to suffer violence and insult on the street, to see mistrust and fear in every eye: this is the lot of the tiefling. And to twist the knife, tieflings know that this is because a pact struck generations ago infused their bloodline with infernal essence. Their appearance and their nature are not their fault but the result of an ancient sin, for which they and their children and their children's children will always be held accountable. Tieflings know that they have to make their own way in the world and that they have to be strong to survive. They are not quick to trust anyone who claims to be a friend, but when a tiefling's companions demonstrate that they trust him or her, a tiefling learns to extend the same trust to them.

---

As a tiefling, you gain the following traits:

**Ability Score Increase:** Intelligence and Charisma 

**Languages:** Common and Infernal 

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Darkvision:** Thanks to your infernal heritage, you have superior vision in dark and dim conditions. You can see in darkness as if it were dim light.  
 
**Fury:** When you damage a creature with an attack or a spell, you can cause the attack or spell to deal extra damage to the creature. The extra damage equals your level. Once you use this trait, you can't use it again until you finish a short or long rest. 

---

## Gallery
<div class="gallery">

![Tiefling 1](img/races/tiefling_1.jpeg)
![Tiefling 2](img/races/tiefling_2.jpg)
![Tiefling 3](img/races/tiefling_3.png)

</div>