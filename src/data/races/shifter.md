Shifters are sometimes called the weretouched, as many believe that they are the descendants of humans and lycanthropes. They are humanoids with a bestial aspect. While they can't fully change shape, they can temporarily enhance their animalistic features—a state they call shifting. Whatever their origins, shifters have evolved into a unique race. A shifter walks on the knife's edge between the wilds and the world around them. Do they embrace their primal instincts or the path of civilization?  
The traits of the beast within affect a shifter's appearance as well. Some shifters might have catlike eyes and delicate build, while others are brutes built like a bear. While a shifter's appearance might remind an onlooker of an animal, they remain clearly identifiable as shifters even when at their most feral.  
While they form powerful bonds with friends and kin, shifters place great value on selfreliance and freedom. It's a shifter proverb to "always be prepared for the journey yet to come," and most shifters strive to be ready for change or opportunity.

---

As a shifter, you gain the following traits:

**Ability Score Increase:** Strength and Dexterity 

**Languages:** Common, and one language of your choice  

**Size:** Your size is medium  

**Speed:** Your base walking speed is 35 feet   

**Honed Instincts:** You gain proficiency in the Survival skill.  
 
**Shifting:** On your turn, you can shift as an action. Shifting lasts until you shift again. While shifting, you gain a feature that depends on the chosen type of shifting:

* _Beasthide:_ You gain a +1 bonus to your AC.  

* _Longstride:_ Your speed increases by 5 feet.

* _Razorclaw:_ Your unarmed attacks deal 1d6 slashing damage.

* _Wildhunt:_ You can see in darkness as if it were dim light. 
