Aasimar bear within their souls the light of the heavens. They are descended from humans with a touch of the power of divinity. Aasimar are born to serve as champions of the gods, their births hailed as blessed events. They are a people of otherworldly visages, with luminous features that reveal their celestial heritage. Some however, prefer to keep a low profile. An aasimar inevitably draws the attention of evil cultists, fiends, and other enemies of good, all of whom would be eager to strike down a celestial champion if they had the chance. They nevertheless have no compunction about striking openly at evil. The secrecy they desire is never worth endangering the innocent.

---

As an aasimar, you gain the following traits:

**Ability Score Increase:** Wisdom and Charisma 

**Languages:** Common and Celestial 

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Darkvision:** Thanks to your celestial heritage, you have superior vision in dark and dim conditions. You can see in darkness as if it were dim light.  
 
**Healing Touch:** As an action, you can touch a creature and cause it to regain a number of hit points equal to your level. Once you use this trait, you cannot use it again until you finish a short or long rest.   

---

## Gallery
<div class="gallery">

![Aasimar 1](img/races/aasimar_1.jpeg)
![Aasimar 2](img/races/aasimar_2.jpg)
![Aasimar 3](img/races/aasimar_3.jpg)

</div>
