Kingdoms rich in ancient grandeur, halls carved into the roots of mountains, the echoing of picks and hammers in deep mines and blazing forges, a commitment to clan and tradition; these common threads unite all dwarves. They are solid and enduring like the mountains they love, weathering the passage of centuries with stoic endurance and little change. They respect the traditions of their clans, tracing their ancestry back to the founding of their most ancient strongholds in the youth of the world, and don't abandon those traditions lightly. Individual dwarves are determined and loyal, true to their word and decisive in action, sometimes to the point of stubbornness. Many dwarves have a strong sense of justice, and they are slow to forget wrongs they have suffered.

---

As a dwarf, you gain the following traits:

**Ability Score Increase:** Constitution and Wisdom  

**Languages:** Common and Dwarven 

**Size:** Your size is medium  

**Speed:** Your base walking speed is 25 feet   

**Dwarven Toughness:** Your hit point maximum increases by 2, and it increases by 2 every time you gain a level  
 
**Steadfast:** Your speed is not reduced by wearing heavy armor.   

---

## Gallery
<div class="gallery">

![Dwarf 1](img/races/dwarf_1.jpg)
![Dwarf 2](img/races/dwarf_2.jpg)
![Dwarf 3](img/races/dwarf_3.jpg)

</div>
