Born of dragons, as their name proclaims, the dragonborn walk proudly through a world that greets them with fearful incomprehension. Shaped by unknown means, dragonborn originally hatched from dragon eggs as a unique race, combining the best attributes of dragons and humanoids. Some dragonborn are faithful servants to true dragons, others form the ranks of soldiers in great wars, and still others find themselves adrift, with no clear calling in life.  
A continual drive for self-improvement reflects the self-sufficiency, of the race as a whole. Dragonborn value skill and excellence in all endeavors. They hate to fail, and they push themselves to extreme efforts before they give up on something. A dragonborn holds mastery of that particular skill as a lifetime goal. Members of other races who share the same commitment find it easy to earn the respect of a dragonborn.

You have draconic ancestry. Choose one type of dragon from the table below. The dragon type, as shown in the table, determines your Breath Weapon feature and Draconic Resistance feature.

| Dragon | Breath Weapon & Resistance |
|--------|----------------------------|
| Black or Copper | Acid |
| Blue or Bronze | Lightning |
| Green or Brass | Poison |
| Red or Gold | Fire |
| White or Silver | Cold |

---

As a dragonborn, you gain the following traits:

**Ability Score Increase:** Strength and Intelligence  

**Languages:** Common and Draconic  

**Size:** Your size is medium  

**Speed:** Your base walking speed is 30 feet   

**Breath Weapon:** You can use your action to exhale destructive energy.  When you use your Breath Weapon, each creature in a 15 feet cone in front of you must make a Dexterity saving throw. The DC for this saving throw equals 13 + your proficiency bonus. A creature takes 2d6 damage on a failed save, and half as much damage on a successful one. Your draconic ancestry determines the damage type of the exhalation. The damage increases by 2d6 at 3rd level and by additional 2d6 every two levels thereafter. After you use your Breath Weapon, you can’t use it again until you finish a short or long rest.
 
**Draconic Resistance:** You have resistance to the damage type associated with your draconic ancestry.   

---

## Gallery
<div class="gallery">

![Dragonborn 1](img/races/dragonborn_1.jpg)
![Dragonborn 2](img/races/dragonborn_2.jpg)
![Dragonborn 3](img/races/dragonborn_3.jpg)

</div>
