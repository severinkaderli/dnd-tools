import Artificer from "./Artificer";
import Barbarian from "./Barbarian";
import Bard from "./Bard";
import Cleric from "./Cleric";
import Druid from "./Druid";
import Fighter from "./Fighter";
import Monk from "./Monk";
import Paladin from "./Paladin";
import Rogue from "./Rogue";
import Sorcerer from "./Sorcerer";
import Warlock from "./Warlock";
import Wizard from "./Wizard";

export default {
  Artificer,
  Barbarian,
  Bard,
  Cleric,
  Druid,
  Fighter,
  Monk,
  Paladin,
  Rogue,
  Sorcerer,
  Warlock,
  Wizard
};
