import general from "./markdown/sorcerer.md";
import classFeatures from "./markdown/sorcerer.features.md";
import info from "./markdown/sorcerer.info.md";

export default {
  general,
  classFeatures,
  info,
  subclasses: [
    "Draconic Bloodline",
    "Phoenix Bloodline",
    "Vampiric Bloodline"
  ],
  columns: [
    {
      field: "level",
      label: "Level",
      centered: true
    },
    {
      field: "bonus",
      label: "Prof. Bonus",
      centered: true
    },
    {
      field: "classFeatures",
      label: "Class Features"
    },
    {
      field: "cantrips",
      label: "Cantrips Known",
      centered: true
    },
    {
      field: "spells",
      label: "Spells Known",
      centered: true
    },
    {
      field: "first",
      label: "1st",
      centered: true
    },
    {
      field: "second",
      label: "2nd",
      centered: true
    },
    {
      field: "third",
      label: "3rd",
      centered: true
    },
    {
      field: "fourth",
      label: "4th",
      centered: true
    },
    {
      field: "fifth",
      label: "5th",
      centered: true
    },
    {
      field: "sixth",
      label: "6th",
      centered: true
    },
    {
      field: "seventh",
      label: "7th",
      centered: true
    },
    {
      field: "eighth",
      label: "8th",
      centered: true
    },
    {
      field: "ninth",
      label: "9th",
      centered: true
    }
  ],
  data: [
    {
      level: 1,
      bonus: "+2",
      classFeatures: "Spellcasting, Living Focus, Sorcerous Bloodline",
      cantrips: 2,
      spells: 3,
      first: 2,
      second: "-",
      third: "-",
      fourth: "-",
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 2,
      bonus: "+2",
      classFeatures: "Bloodline Feature, Metamagic",
      cantrips: 2,
      spells: 4,
      first: 3,
      second: "-",
      third: "-",
      fourth: "-",
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 3,
      bonus: "+2",
      classFeatures: "Magical Burn",
      cantrips: 2,
      spells: 6,
      first: 4,
      second: 2,
      third: "-",
      fourth: "-",
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 4,
      bonus: "+2",
      classFeatures: "Ability Score Improvement",
      cantrips: 2,
      spells: 7,
      first: 4,
      second: 3,
      third: "-",
      fourth: "-",
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 5,
      bonus: "+3",
      classFeatures: "--",
      cantrips: 2,
      spells: 9,
      first: 4,
      second: 3,
      third: 2,
      fourth: "-",
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 6,
      bonus: "+3",
      classFeatures: "Bloodline Feature",
      cantrips: 3,
      spells: 10,
      first: 4,
      second: 3,
      third: 3,
      fourth: "-",
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 7,
      bonus: "+3",
      classFeatures: "--",
      cantrips: 3,
      spells: 12,
      first: 4,
      second: 3,
      third: 3,
      fourth: 1,
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 8,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      cantrips: 3,
      spells: 13,
      first: 4,
      second: 3,
      third: 3,
      fourth: 2,
      fifth: "-",
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 9,
      bonus: "+4",
      classFeatures: "--",
      cantrips: 3,
      spells: 15,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 1,
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 10,
      bonus: "+4",
      classFeatures: "Bloodline Feature, Metamagic Improvement",
      cantrips: 3,
      spells: 16,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 2,
      sixth: "-",
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 11,
      bonus: "+4",
      classFeatures: "--",
      cantrips: 4,
      spells: 18,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 12,
      bonus: "+4",
      classFeatures: "Ability Score Improvement",
      cantrips: 4,
      spells: 19,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: "-",
      eighth: "-",
      ninth: "-"
    },
    {
      level: 13,
      bonus: "+5",
      classFeatures: "--",
      cantrips: 4,
      spells: 21,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: 1,
      eighth: "-",
      ninth: "-"
    },
    {
      level: 14,
      bonus: "+5",
      classFeatures: "Bloodline Feature",
      cantrips: 4,
      spells: 22,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: 1,
      eighth: "-",
      ninth: "-"
    },
    {
      level: 15,
      bonus: "+5",
      classFeatures: "--",
      cantrips: 4,
      spells: 24,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: 1,
      eighth: 1,
      ninth: "-"
    },
    {
      level: 16,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      cantrips: 5,
      spells: 25,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: 1,
      eighth: 1,
      ninth: "-"
    },
    {
      level: 17,
      bonus: "+6",
      classFeatures: "--",
      cantrips: 5,
      spells: 27,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 1,
      seventh: 1,
      eighth: 1,
      ninth: 1
    },
    {
      level: 18,
      bonus: "+6",
      classFeatures: "Bloodline Feature",
      cantrips: 5,
      spells: 28,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 2,
      seventh: 1,
      eighth: 1,
      ninth: 1
    },
    {
      level: 19,
      bonus: "+6",
      classFeatures: "Ability Score Improvement",
      cantrips: 5,
      spells: 30,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 3,
      seventh: 2,
      eighth: 1,
      ninth: 1
    },
    {
      level: 20,
      bonus: "+6",
      classFeatures: "Master of Magic",
      cantrips: 5,
      spells: 30,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 3,
      sixth: 3,
      seventh: 3,
      eighth: 2,
      ninth: 1
    }
  ]
};
