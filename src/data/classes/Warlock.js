import general from "./markdown/warlock.md";
import classFeatures from "./markdown/warlock.features.md";
import info from "./markdown/warlock.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: ["Infernal Patron", "Old One Patron", "Shadowfell Patron"],
  columns: [
    {
      field: "level",
      label: "Level",
      centered: true
    },
    {
      field: "bonus",
      label: "Prof. Bonus",
      centered: true
    },
    {
      field: "classFeatures",
      label: "Class Features"
    },
    {
      field: "invocations",
      label: "Invocations Known"
    },
    {
      field: "spells",
      label: "Spells Known"
    },
    {
      field: "first",
      label: "1st"
    },
    {
      field: "second",
      label: "2nd"
    },
    {
      field: "third",
      label: "3rd"
    },
    {
      field: "fourth",
      label: "4th"
    },
    {
      field: "fifth",
      label: "5th"
    }
  ],
  data: [
    {
      level: 1,
      bonus: "+2",
      classFeatures: "Pact Weapon, Spellcasting, Warlock Pact",
      invocations: "-",
      spells: 2,
      first: 2,
      second: "-",
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 2,
      bonus: "+2",
      classFeatures: "Fighting Style, Invocations",
      invocations: 1,
      spells: 2,
      first: 2,
      second: "-",
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 3,
      bonus: "+2",
      classFeatures: "Pact Feature",
      invocations: 1,
      spells: 3,
      first: 3,
      second: "-",
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 4,
      bonus: "+2",
      classFeatures: "Ability Score Improvement",
      invocations: 1,
      spells: 3,
      first: 3,
      second: "-",
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 5,
      bonus: "+3",
      classFeatures: "Extra Attack",
      invocations: 2,
      spells: 5,
      first: 4,
      second: 2,
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 6,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      invocations: 2,
      spells: 5,
      first: 4,
      second: 2,
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 7,
      bonus: "+3",
      classFeatures: "Pact Feature",
      invocations: 2,
      spells: 6,
      first: 4,
      second: 3,
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 8,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      invocations: 2,
      spells: 6,
      first: 4,
      second: 3,
      third: "-",
      fourth: "-",
      fifth: "-"
    },
    {
      level: 9,
      bonus: "+4",
      classFeatures: "--",
      invocations: 3,
      spells: 8,
      first: 4,
      second: 3,
      third: 2,
      fourth: "-",
      fifth: "-"
    },
    {
      level: 10,
      bonus: "+4",
      classFeatures: "Hexblade",
      invocations: 3,
      spells: 8,
      first: 4,
      second: 3,
      third: 2,
      fourth: "-",
      fifth: "-"
    },
    {
      level: 11,
      bonus: "+4",
      classFeatures: "Pact Feature",
      invocations: 3,
      spells: 9,
      first: 4,
      second: 3,
      third: 3,
      fourth: "-",
      fifth: "-"
    },
    {
      level: 12,
      bonus: "+4",
      classFeatures: "Ability Score Improvement",
      invocations: 3,
      spells: 9,
      first: 4,
      second: 3,
      third: 3,
      fourth: "-",
      fifth: "-"
    },
    {
      level: 13,
      bonus: "+5",
      classFeatures: "--",
      invocations: 4,
      spells: 11,
      first: 4,
      second: 3,
      third: 3,
      fourth: 1,
      fifth: "-"
    },
    {
      level: 14,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      invocations: 4,
      spells: 11,
      first: 4,
      second: 3,
      third: 3,
      fourth: 1,
      fifth: "-"
    },
    {
      level: 15,
      bonus: "+5",
      classFeatures: "Pact Feature",
      invocations: 4,
      spells: 12,
      first: 4,
      second: 3,
      third: 3,
      fourth: 2,
      fifth: "-"
    },
    {
      level: 16,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      invocations: 4,
      spells: 12,
      first: 4,
      second: 3,
      third: 3,
      fourth: 2,
      fifth: "-"
    },
    {
      level: 17,
      bonus: "+6",
      classFeatures: "--",
      invocations: 5,
      spells: 14,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 1
    },
    {
      level: 18,
      bonus: "+6",
      classFeatures: "Mystic Arcanum",
      invocations: 5,
      spells: 14,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 1
    },
    {
      level: 19,
      bonus: "+6",
      classFeatures: "Pact Feature",
      invocations: 5,
      spells: 15,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 2
    },
    {
      level: 20,
      bonus: "+6",
      classFeatures: "Pact Master",
      invocations: 5,
      spells: 15,
      first: 4,
      second: 3,
      third: 3,
      fourth: 3,
      fifth: 2
    }
  ]
};
