import general from "./markdown/artificer.md";
import classFeatures from "./markdown/artificer.features.md";
import info from "./markdown/artificer.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: [],
  columns: [],
  data: []
};
