import general from "./markdown/barbarian.md";
import classFeatures from "./markdown/barbarian.features.md";
import info from "./markdown/barbarian.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: [
    "Path of the Berserker",
    "Path of the Guardian",
    "Path of the Slayer"
  ],
  columns: [
    {
      field: "level",
      label: "Level",
      centered: true
    },
    {
      field: "bonus",
      label: "Prof. Bonus",
      centered: true
    },
    {
      field: "classFeatures",
      label: "Class Features"
    },
    {
      field: "rageBonus",
      label: "Rage Bonus"
    }
  ],
  data: [
    {
      level: 1,
      bonus: "+2",
      classFeatures: "Rage, Feral Instincts, Barbarian Path",
      rageBonus: "+2"
    },
    {
      level: 2,
      bonus: "+2",
      classFeatures: "Fighting Style, Fast Movement",
      rageBonus: "+2"
    },
    {
      level: 3,
      bonus: "+2",
      classFeatures: "Path Feature",
      rageBonus: "+2"
    },
    {
      level: 4,
      bonus: "+2",
      classFeatures: "Ability Score Improvement",
      rageBonus: "+2"
    },
    {
      level: 5,
      bonus: "+3",
      classFeatures: "Extra Attack",
      rageBonus: "+2"
    },
    {
      level: 6,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      rageBonus: "+3"
    },
    {
      level: 7,
      bonus: "+3",
      classFeatures: "Path Feature",
      rageBonus: "+3"
    },
    {
      level: 8,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      rageBonus: "+3"
    },
    {
      level: 9,
      bonus: "+4",
      classFeatures: "Brutal Critical",
      rageBonus: "+3"
    },
    {
      level: 10,
      bonus: "+4",
      classFeatures: "Retaliation",
      rageBonus: "+3"
    },
    {
      level: 11,
      bonus: "+4",
      classFeatures: "Path Feature",
      rageBonus: "+4"
    },
    {
      level: 12,
      bonus: "+4",
      classFeatures: "Ability Score Improvement",
      rageBonus: "+4"
    },
    {
      level: 13,
      bonus: "+5",
      classFeatures: "Brutal Critical (2)",
      rageBonus: "+4"
    },
    {
      level: 14,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      rageBonus: "+4"
    },
    {
      level: 15,
      bonus: "+5",
      classFeatures: "Path Feature",
      rageBonus: "+4"
    },
    {
      level: 16,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      rageBonus: "+5"
    },
    {
      level: 17,
      bonus: "+6",
      classFeatures: "Brutal Critical (3)",
      rageBonus: "+5"
    },
    {
      level: 18,
      bonus: "+6",
      classFeatures: "Might",
      rageBonus: "+5"
    },
    {
      level: 19,
      bonus: "+6",
      classFeatures: "Path Feature",
      rageBonus: "+5"
    },
    {
      level: 20,
      bonus: "+6",
      classFeatures: "Primal Champion",
      rageBonus: "+5"
    }
  ]
};
