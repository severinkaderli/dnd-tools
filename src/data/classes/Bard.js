import general from "./markdown/bard.md";
import classFeatures from "./markdown/bard.features.md";
import info from "./markdown/bard.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: [],
  columns: [],
  data: []
};
