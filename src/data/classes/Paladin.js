import general from "./markdown/paladin.md";
import classFeatures from "./markdown/paladin.features.md";
import info from "./markdown/paladin.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: [],
  columns: [],
  data: []
};
