import general from "./markdown/cleric.md";
import classFeatures from "./markdown/cleric.features.md";
import info from "./markdown/cleric.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: [],
  columns: [],
  data: []
};
