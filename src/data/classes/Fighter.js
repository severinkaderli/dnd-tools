import general from "./markdown/fighter.md";
import classFeatures from "./markdown/fighter.features.md";
import info from "./markdown/fighter.info.md";

export default {
  general,
  info,
  classFeatures,
  subclasses: [],
  columns: [],
  data: []
};
