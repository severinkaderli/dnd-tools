import general from "./markdown/rogue.md";
import classFeatures from "./markdown/rogue.features.md";
import info from "./markdown/rogue.info.md";

export default {
  general,
  classFeatures,
  info,
  subclasses: [
    "Assassin",
    "Thief",
    "Scout"
  ],
  columns: [
    {
      field: "level",
      label: "Level",
      centered: true
    },
    {
      field: "bonus",
      label: "Prof. Bonus",
      centered: true
    },
    {
      field: "classFeatures",
      label: "Class Features"
    },
    {
      field: "sneakAttack",
      label: "Sneak Attack",
      centered: true
    }
  ],
  data: [
    {
      level: 1,
      bonus: "+2",
      classFeatures: "Sneak Attack, Cunning Action, Rogue Archetype",
      sneakAttack: "1d6"
    },
    {
      level: 2,
      bonus: "+2",
      classFeatures: "Expertise, Uncanny Dodge",
      sneakAttack: "1d6"
    },
    {
      level: 3,
      bonus: "+2",
      classFeatures: "Archetype Feature",
      sneakAttack: "2d6"
    },
    {
      level: 4,
      bonus: "+2",
      classFeatures: "Ability Score Improvement",
      sneakAttack: "2d6"
    },
    {
      level: 5,
      bonus: "+3",
      classFeatures: "Extra Attack",
      sneakAttack: "3d6"
    },
    {
      level: 6,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      sneakAttack: "3d6"
    },
    {
      level: 7,
      bonus: "+3",
      classFeatures: "Archetype Feature",
      sneakAttack: "4d6"
    },
    {
      level: 8,
      bonus: "+3",
      classFeatures: "Ability Score Improvement",
      sneakAttack: "4d6"
    },
    {
      level: 9,
      bonus: "+4",
      classFeatures: "Eye for Weakness",
      sneakAttack: "5d6"
    },
    {
      level: 10,
      bonus: "+4",
      classFeatures: "Evasion",
      sneakAttack: "5d6"
    },
    {
      level: 11,
      bonus: "+4",
      classFeatures: "Archetype Feature",
      sneakAttack: "6d6"
    },
    {
      level: 12,
      bonus: "+4",
      classFeatures: "Ability Score Improvement",
      sneakAttack: "6d6"
    },
    {
      level: 13,
      bonus: "+5",
      classFeatures: "Reliable Talent",
      sneakAttack: "7d6"
    },
    {
      level: 14,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      sneakAttack: "7d6"
    },
    {
      level: 15,
      bonus: "+5",
      classFeatures: "Archetype Feature",
      sneakAttack: "8d6"
    },
    {
      level: 16,
      bonus: "+5",
      classFeatures: "Ability Score Improvement",
      sneakAttack: "8d6"
    },
    {
      level: 17,
      bonus: "+6",
      classFeatures: "Eye for Weakness (2)",
      sneakAttack: "9d6"
    },
    {
      level: 18,
      bonus: "+6",
      classFeatures: "Elusive",
      sneakAttack: "9d6"
    },
    {
      level: 19,
      bonus: "+6",
      classFeatures: "Archetype Feature",
      sneakAttack: "10d6"
    },
    {
      level: 20,
      bonus: "+6",
      classFeatures: "Most Wanted",
      sneakAttack: "10d6"
    }
  ]
};
