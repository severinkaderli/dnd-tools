import general from "./markdown/monk.md";
import classFeatures from "./markdown/monk.features.md";
import info from "./markdown/monk.info.md";

export default {
  general,
  classFeatures,
  info,
  subclasses: [
    "Tradition of the Open Hand",
    "Tradition of the Hidden Blade",
    "Tradition of the Immortal Soul"
  ],
  columns: [
    {
      field: "level",
      label: "Level",
      centered: true
    },
    {
      field: "bonus",
      label: "Prof. Bonus",
      centered: true
    },
    {
      field: "classFeatures",
      label: "Class Features"
    }
  ],
  data: [
    {
      level: 1,
      bonus: "+2",
      classFeatures: "Martial Arts, Unarmored Defense, Monk Tradition"
    },
    {
      level: 2,
      bonus: "+2",
      classFeatures: "Ki, Unarmored Movement"
    },
    {
      level: 3,
      bonus: "+2",
      classFeatures: "Tradition Feature"
    },
    {
      level: 4,
      bonus: "+2",
      classFeatures: "Ability Score Improvement"
    },
    {
      level: 5,
      bonus: "+3",
      classFeatures: "Extra Attack"
    },
    {
      level: 6,
      bonus: "+3",
      classFeatures: "Ability Score Improvement"
    },
    {
      level: 7,
      bonus: "+3",
      classFeatures: "Tradition Feature"
    },
    {
      level: 8,
      bonus: "+3",
      classFeatures: "Ability Score Improvement"
    },
    {
      level: 9,
      bonus: "+4",
      classFeatures: "Martial Arts Improvement"
    },
    {
      level: 10,
      bonus: "+4",
      classFeatures: "Evasion"
    },
    {
      level: 11,
      bonus: "+4",
      classFeatures: "Tradition Feature"
    },
    {
      level: 12,
      bonus: "+4",
      classFeatures: "Ability Score Improvement"
    },
    {
      level: 13,
      bonus: "+5",
      classFeatures: "Purity of Body"
    },
    {
      level: 14,
      bonus: "+5",
      classFeatures: "Ability Score Improvement"
    },
    {
      level: 15,
      bonus: "+5",
      classFeatures: "Tradition Feature"
    },
    {
      level: 16,
      bonus: "+5",
      classFeatures: "Ability Score Improvement"
    },
    {
      level: 17,
      bonus: "+6",
      classFeatures: "Martial Arts Improvement"
    },
    {
      level: 18,
      bonus: "+6",
      classFeatures: "Purity of Mind"
    },
    {
      level: 19,
      bonus: "+6",
      classFeatures: "Tradition Feature"
    },
    {
      level: 20,
      bonus: "+6",
      classFeatures: "Transcendent Master"
    }
  ]
};
