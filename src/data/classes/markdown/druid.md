## Hit Points
**Hit Dice:** 1d8 per Druid level  
**Hit Points at 1st level:** 8 + your Constitution modifier  
**Hit Points at higher levels:** 1d8 (or 4) + your Constitution
modifier per Druid level after the 1st

## Starting Proficiencies
You are proficient with the following items, in addition to any
proficiencies provided by your race or background:

**Saving Throw:** Wisdom and Intelligence  
**Armor:** light armor, shields  
**Weapons:** simple weapons  
**Tools:** herbalism tools and one tool of your choice  
**Languages:** Druidic  
**Skills:** Nature and two skills of your choice

## Starting Equipment
You start with the following items, plus anything provided by
your background:

* a simple melee weapon
* hide armor and a shield
* an explorer’s pack
* a druidic focus 
