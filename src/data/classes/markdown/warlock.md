## Hit Points
**Hit Dice:** 1d8 per Warlock level  
**Hit Points at 1st level:** 8 + your Constitution modifier  
**Hit Points at higher levels:** 1d8 (or 4) + your Constitution modifier per Warlock level after the 1st

## Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Intelligence and Wisdom  
**Armor:** light armor, medium armor  
**Weapons:** simple weapons  
**Tools:** none  
**Languages:** none  
**Skills:** two skills of your choice

## Starting Equipment
You start with the following items, plus anything provided by your background:

* a simple melee weapon
* leather armor or chain shirt or chain mail (if proficient)
* a scholar’s pack or a dungeoneer’s pack
