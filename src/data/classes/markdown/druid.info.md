Whether calling on the elemental forces of nature or emulating the creatures of the animal world, druids are an embodiment of nature's resilience, cunning, and fury. They claim no mastery over nature. Instead, they see themselves as extensions of nature's indomitable will. They are the caretakers of the natural world, and it is said that sometimes a druid becomes the voice of nature, speaking the truth that is too subtle for the general populace to hear. Many who become druids find that they naturally gravitate toward nature; its forces, cycles, and movements fill their minds and spirits with wonder and insight.

Druids revere nature above all, gaining their spells and other magical powers from the primal force of nature itself. Many druids pursue a mystic spirituality of transcendent union with nature rather than devotion to a divine entity, while others serve gods of wild nature, animals, or elemental forces. They are often found guarding sacred sites or watching over regions of unspoiled nature. But when a significant danger arises, threatening nature's balance or the lands they protect, druids take on a more active role in combating the threat, as adventurers.

When making a druid, consider why your character has such a close bond with nature. Perhaps your character lives in a society where the such beliefs still thrive. Maybe your character had a dramatic encounter with the spirits of nature, coming face to face with danger and surviving the experience. Whatever the reason, nature is part of your character's destiny. 

_Even in death, each creature plays its part in maintaining the Great Balance. As druids, we seek mainly to protect and educate, to preserve the Great Balance._

### Gallery
<div class="gallery">

![Druid 1](img/classes/druid_1.jpg)
![Druid 2](img/classes/druid_2.png)
![Druid 3](img/classes/druid_3.jpg)
![Druid 4](img/classes/druid_4.png)
![Druid 5](img/classes/druid_5.jpg)
![Druid 6](img/classes/druid_6.jpg)

</div>
