### Hit Points
**Hit Dice:** 1d6 per Wizard level  
**Hit Points at 1st level:** 6 + your Constitution modifier  
**Hit Points at higher levels:** 1d6 (or 3) + your Constitution modifier per Wizard level after the 1st

### Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Intelligence and Wisdom  
**Armor:** none  
**Weapons:** none  
**Tools:** one tool of your choice  
**Languages:** one language of your choice  
**Skills:** Arcana and two skills of your choice  

### Starting Equipment
You start with the following items, plus anything provided by your background:

* a scholar's pack or an explorer's pack  
* a spellbook of your design