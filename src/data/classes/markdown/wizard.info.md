Wizards are supreme magic-users, defined and united as a class by the spells they cast. Drawing on the subtle weave of magic that permeates the cosmos, wizards cast spells of explosive fire, arcing lightning, subtle deception, and brute-force mind control. Their mightiest spells change one substance into another, call meteors down from the sky, or open portals to other worlds.

The lure of knowledge and power calls even the most unadventurous wizards out of the safety of their libraries and laboratories and into crumbling ruins and lost cities. Most wizards believe that their counterparts in ancient civilizations knew secrets of magic that have been lost to the ages, and discovering those secrets could unlock the path to a power greater than any magic available in the present age.

Creating a wizard character demands a backstory dominated by at least one extraordinary event. How did your character first come into contact with magic and why did you decide to study it? And what drew you forth from your life of study to become an adventurer?

## Gallery
<div class="gallery">

![Wizard 1](img/classes/wizard_1.jpg)
![Wizard 2](img/classes/wizard_2.png)
![Wizard 3](img/classes/wizard_3.jpg)
![Wizard 4](img/classes/wizard_4.jpg)
![Wizard 5](img/classes/wizard_5.jpg)
![Wizard 6](img/classes/wizard_6.jpg)

</div>