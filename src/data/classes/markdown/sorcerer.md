# General

## Hit Points
**Hit Dice:** 1d6 per Sorcerer level  
**Hit Points at 1st level:** 6 + your Constitution modifier  
**Hit Points at higher levels:** 1d6 (or 3) + your Constitution modifier per Sorcerer level after the 1st  

## Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Constitution and Charisma   
**Armor:** none  
**Weapons:** none  
**Tools:** one tool of your choice   
**Languages:** none   
**Skills:** two skills of your choice  

## Starting Equipment
You start with the following items, plus anything provided by your background:

* a dungeoneer’s pack¨or an explorer's pack
