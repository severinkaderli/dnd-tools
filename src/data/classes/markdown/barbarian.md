## Hit Points
**Hit Dice:** 1d12 per Barbarian level  
**Hit Points at 1st level:** 12 + your Constitution modifier  
**Hit Points at higher levels:** 1d12 (or 6) + your Constitution modifier per Barbarian level after the 1st  

## Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Strength and Constitution  
**Armor:** light armor, medium armor, shields  
**Weapons:** simple weapons, martial weapons  
**Tools:** none  
**Languages:** none  
**Skills:** Athletics and one skill of your choice  

## Starting Equipment
You start with the following items, plus anything provided by your background:

* a martial melee weapon  
* a shield or another martial melee weapon  
* a martial ranged weapon  
* leather armor or a chain shirt  
* an explorer’s pack  

