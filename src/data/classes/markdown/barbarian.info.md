Barbarians are defined by their rage: unbridled, unquenchable, and unthinking fury. More than a mere emotion, their anger is the ferocity of a cornered predator, the unrelenting assault of a storm, the churning turmoil of the sea. For every barbarian, rage is a power that fuels not just a battle frenzy but also uncanny reflexes, resilience, and feats of strength.

Their courage in the face of danger makes barbarians perfectly suited for adventuring. Wandering is often well suited for them, as the rootless life of the adventurer is little hardship for a barbarian. Some chase this path, to form strong bonds among members of their parties.

When creating a barbarian character, think about where your character comes from and his or her place in the world. What led you to take up the adventuring life? It can be tempting to play a barbarian character that is a straightforward application of the classic archetype — a brute, and usually a dimwitted one at that, who rushes in where others fear to tread. But not all the barbarians in the world are cut from that cloth, so consider adding some flourishes to make your barbarian stand out from all others.

_I have witnessed the indomitable performance of barbarians on the field of battle, and it makes me wonder what force lies at the heart of their rage._

## Gallery
<div class="gallery">

![Barbarian 1](img/classes/barbarian_1.jpg)
![Barbarian 2](img/classes/barbarian_2.jpg)
![Barbarian 3](img/classes/barbarian_3.jpg)
![Barbarian 4](img/classes/barbarian_4.png)
![Barbarian 5](img/classes/barbarian_5.jpg)
![Barbarian 6](img/classes/barbarian_6.jpg)

</div>
