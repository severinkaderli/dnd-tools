Rogues rely on skill, stealth, and their foes' vulnerabilities to get the upper hand in any situation. They have a knack for finding the solution to just about any problem, demonstrating a resourcefulness and versatility that is the cornerstone of any successful adventuring party.

Rogues devote as much effort to mastering the use of a variety of skills as they do to perfecting their combat abilities, giving them a broad expertise that few other characters can match. Many rogues focus on stealth and deception, while others refine the skills that help them in a dungeon environment, such as climbing, finding and disarming traps, and opening locks.

When it comes to combat, rogues prioritize cunning over brute strength. A rogue would rather make one precise strike, placing it exactly where the attack will hurt the target most, than wear an opponent down with a barrage of attacks. 

Some rogues who turn to adventuring are former criminals who have decided that dodging monsters is preferable to remaining one step ahead of the law. Others are professional killers in search of a profitable application of their talents between contracts. Some simply love the thrill of overcoming any challenge that stands in their way.

As you create your rogue character, consider the character's relationship to the law. Do you have a criminal past or present? Is it greed that drives you in your adventures, or some other desire or ideal?

## Gallery
<div class="gallery">

![Rogue 1](img/classes/rogue_1.jpg)
![Rogue 2](img/classes/rogue_2.jpg)
![Rogue 3](img/classes/rogue_3.jpg)
![Rogue 4](img/classes/rogue_4.jpg)
![Rogue 5](img/classes/rogue_5.jpg)
![Rogue 6](img/classes/rogue_6.jpg)

</div>
