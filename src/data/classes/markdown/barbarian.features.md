## Rage
At 1st level, you fight with primal ferocity in battle. On your turn, you can enter a rage as a bonus action. While raging, you gain the following benefits:

* When you make an attack using Strength, you gain a bonus to the damage roll that increases as you gain levels as a Barbarian, as shown in the rage bonus column of the Barbarian table.  
* You have resistance to non-magical bludgeoning, piercing, and slashing damage.  
* You have advantage on Strength checks and Strength saving throws.  

Your rage lasts for 1 minute. It ends early if you are knocked unconscious. You can also end your rage on your turn as a bonus action. Once you have raged a number of times equal to  your Strength modifier, you must finish a short or long rest before you can rage again.

## Feral Instincts
At 1st level, the nature of your barbarian vocation has trained your senses and allows you to quickly react in battle. You have advantage on Initiative rolls. 

## Barbarian Path
At 1st level, you choose a path that shapes the nature of your rage: you either fully give in to become a berserker, or you connect with  natural spirits and your ancestors to join the ranks of guardians, or you channel it towards your ability to hunt the most dangerous creatures as a slayer. The path you choose grants you features at 1st level and again at 3rd, 7th, 11th, 15th and 19th level.

<div class="path-of-the-berserker">
<blockquote>

## Painbringer
At 1st level, you accept rage as a means to an end, and that end is violence. As you walk this path, slick with blood, you trill in the chaos of battle, heedless of your own health and filled with untrammeled fury. Whenever you are raging, your weapon attacks using Strength score a critical hit on a roll of 19 or 20.  
Additionally, whenever you roll for initiative, you can immediately enter your rage as a free action. 

</blockquote>
</div>

<div class="path-of-the-guardian">
<blockquote>

## Spirit Guide
At 1st level, you begin your spiritual journey, as you accept the souls of your ancestors and of sacred guardian spirits to bond with you. They inspire, protect and guide you. You gain proficiency with two skills or artisan tools of your choice. 
	
</blockquote>
</div>

<div class="path-of-the-slayer">
<blockquote>

## Daring Hunter
At 1st level, as a Slayer, you accept your place as a bulwark between civilization and the terrors of the wilderness. As you walk this path, you learn specialized techniques to understand, track and fight the various threats you face. You gain proficiency with the Survival skill.  
Additionally, you have advantage on Survival checks when tracking a creature, and you can precisely learn its size, how long ago it passed through, and how many creatures traveled together.  
	
</blockquote>
</div>

## Fighting Style
At 2nd level, you are trained in martial combat. You can choose a fighting style from the following list. 

<details>
<summary>Fighting Styles</summary>

### Defensive Style
While you are wearing armor, you gain a +1 bonus to AC.

### Dueling Style
When you are wielding a melee weapon in one hand and no other weapons, you gain a +2 bonus to damage rolls with that weapon.

### Great Weapon Style
When you roll a 1 on a damage die for an attack you make with a weapon that has the two-handed property, you can reroll the die and must use the new roll.

### Protective Style
When a creature you can see attacks a target other than you that is within 5 feet of you, you can use your reaction to impose disadvantage on the attack roll. 

### Ranged Weapon Style
You gain a +2 bonus to attack rolls you make with ranged weapons.

### Two-Weapon Style
You can engange in two-weapon fighting even when one of the two melee weapons you are wielding doesn't have the light property.

</details>

## Fast Movement
At 2nd level, you adopt the reckless but fast combat style of barbarians. Your speed increases by 10 feet while you aren't wearing heavy armor.

<div class="path-of-the-berserker">
<blockquote>

## Reckless Attack
At 3rd level, you can throw aside all concern for defense to attack with fierce desperation. When you make your first attack on your turn, you can decide to attack recklessly. Doing so gives you advantage on all attack rolls using Strength during this turn, but attack rolls against you have advantage until the start of your next turn.

</blockquote>
</div>

<div class="path-of-the-guardian">
<blockquote>

## Totem Animal
At 3rd level, whenever you rage, you choose one of the following spirit guides to bond with for the duration of your rage.

### Bear Totem
While you are raging, any creature within 5 feet of you that's hostile to you has disadvantage on attack rolls against targets other than you.

### Eagle Totem
While you are raging, other creatures have disadvantage on opportunity attack rolls against you. Additionally, you can use the Dash action as a bonus action. 

### Wolf Totem
While you are raging, allied creatures have advantage on attack rolls against any creature within 5 feet of you that is hostile to you.

</blockquote>
</div>

<div class="path-of-the-slayer">
<blockquote>

## Favored Enemy
At 3rd level, you have significant experience dealing with a certain category of creatures. Choose one of the options below. Each option gives you a bonus to your attack rolls equal to your rage bonus against certain creature types and grants you a special feature. 

### Colossi  
_Dragons, Giants & Monstrosities_   
As an expert on exotic and highly dangerous creatures, you recognize research as an important aspect on your path. You have advantage on all Intelligence check to gather and recall information about any of your favored enemies. 

### Forlorn  
_Constructs, Fey & Undead_   
Some foes have natural defences against your normal physical attacks. You learned to circumvent these defences. Your weapon attacks ignore resistance against non-magical bludgeoning, slashing and piercing damage. 

### Outsiders  
_Aberrations, Celestials & Infernals_  
You developed a resistance against the corrupting influence of entities, who are far removed from the lives of mortals. You have advantage on saving throws against being charmed and frightened.  

### Kin  
_Humanoids_  
Being able to navigate through different social circles is invaluable to hunt your quarry. You learn two languages of your choice.

### Primal  
_Beasts, Elementals & Plants_  
To chase one’s quarry through the wilderness is what drives you as a slayer. Moving through difficult terrain costs you no extra movement.

You can choose one additional option at 7th level, and again at 11th, 15th and 19th level. 
	
</blockquote>
</div>

## Ability Score Improvement
When you reach 4th level, and again at 6th, 8th, 12th, 14th, and 16th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1, or you can take a feat for which you fulfill the requirements. As normal, you cannot increase an ability score above 20 using this feature.

## Extra Attack
At 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.

<div class="path-of-the-berserker">
<blockquote>

## Power Through Suffering
At 7th level, the sight of your own blood empowers your rage. When you are below half of your hit point maximum, your rage damage bonus is doubled for each attack you make. 

</blockquote>
</div>

<div class="path-of-the-guardian">
<blockquote>

## Spirit Shield
At 7th level, the spirits of your ancestors provide supernatural protection to those you defend. If you are raging and an allied creature you can see within 30 feet of you takes damage, you can use your reaction to reduce the damage by half.
	
</blockquote>
</div>

<div class="path-of-the-slayer">
<blockquote>

## Focused Rage
At 7th level, you learn to focus your rage into an unstopabble fury against one target. When you enter your rage, you can choose one creature you can see. It is considered marked by you until the end of that rage. Your attacks deal twice the normal amount of rage damage against the marked target. 
	
</blockquote>
</div>

## Brutal Critical
At 9th level, you can roll one additional weapon damage die when determining the extra damage for a critical hit with a weapon attack using Strength.  
This increases to two additional dice at 13th level and three additional dice at 17th level.

## Retaliation
At 10th level, when you take damage from a creature that is within range of you, you can use your reaction to make an attack against that creature.

<div class="path-of-the-berserker">
<blockquote>

## Frenzy
At 11th level, you embrace your hatred to go into a frenzy whenever you rage. If you choose to do so, for the duration of this frenized rage, you can attack three times whenever you take the Attack action on your turn. When this rage ends, you suffer one level of exhaustion.

</blockquote>
</div>

<div class="path-of-the-guardian">
<blockquote>

## Consult the Ancients
At 11th level, you gain the ability to consult with your spirits. Right before you make an Intelligence or a Wisdom check, you can give yourself advantage on the check. You can use this feature a number of times equal to your Strength modifier. You regain expended uses when you finish a short or long rest.
	
</blockquote>
</div>

<div class="path-of-the-slayer">
<blockquote>

## Tireless
At 11th level, you develop a remarkable resilience and endurance to further chase down your quarry. Whenever you finish a short or long rest, you can decrease your exhaustion level by one and you gain temporary hit points equal to your barbarian level.
	
</blockquote>
</div>

<div class="path-of-the-berserker">
<blockquote>

## Deadly Perfection
At 15th level, you focus on raw physical power, honed to excellency. While raging, your weapon attacks using Strength score a critical hit on a roll of 18 to 20.

</blockquote>
</div>

<div class="path-of-the-guardian">
<blockquote>

## Totemic Attunement
At 15th level, your spirit guides grant you the following additional magical benefits whenever you rage:

### Bear Totem
While raging, you have resistance to all damage except psychic damage.

### Eagle Totem
While raging, you have a flying speed equal to your current walking speed. This benefit works only in short bursts; you fall if you end your turn in the air and nothing else is holding you aloft.

###	Wolf Totem
While raging, you gain advantage on attack rolls against creatures if at least one allied creature is within 5 feet of the target. 
	
</blockquote>
</div>

<div class="path-of-the-slayer">
<blockquote>

## Predator Sense
At 15th level, you gain the ability to peer at a creature and magically discren how to best hunt it. As a bonus action on your turn, choose a creature you can see. You immediately learn the creature’s resistances, immunities and vulnerabilities. 
	
</blockquote>
</div>

## Might
At 18th level, if your total for a Strength check or saving throw is less than your Strength score, you can use that score in place of the total.

<div class="path-of-the-berserker">
<blockquote>

## Reckoning
At 19th level, you attain the pinnacle of ferocity in battle. You can use your Retaliation feature once per turn without needing to spend your reaction.

</blockquote>
</div>

<div class="path-of-the-guardian">
<blockquote>

## Vengeful Ancestors
At 19th level, your ancestral spirits grow powerful enough to retaliate. When you use your Spirit Shield feature, the attacker takes an amount of force damage equal to the damage that you prevented.
	
</blockquote>
</div>

<div class="path-of-the-slayer">
<blockquote>

## Huntmaster
At 19th level, you become an unparalleled hunter of your enemies. You’ll always know when a creature you can see takes damage that reduces its hit points below half of its maximum.  
Additionally, you gain advantage on weapon attacks using Strength against creatures that are below half of their hit point maximum. 
	
</blockquote>
</div>

## Primal Champion
At 20th level, you embody the power of the wilds. Your Strength and Constitution scores increase by 4. Your maximum for those scores is now 24.