Warlocks are seekers of the knowledge that lies hidden in the fabric of the multiverse. Through pacts made with mysterious beings of supernatural power, warlocks unlock magical effects both subtle and spectacular. Drawing on the ancient knowledge, warlocks piece together arcane secrets to bolster their own power.

Warlocks are finders and keepers of secrets. They push at the edge of our understanding of the world, always seeking to expand their expertise. Where sages or wizards might heed a clear sign of danger and end their research, a warlock plunges ahead, heedless of the cost. Thus, it takes a peculiar mixture of intelligence, curiosity, and recklessness to produce a warlock. Many folk would describe that combination as evidence of madness. Warlocks see it as a demonstration of bravery.

As you make your warlock character, spend some time thinking about your patron and the obligations that your pact imposes upon you. What led you to make the pact, and how did you make contact with your patron? Did you search for your patron, or did your patron find and choose you? Do you chafe under the obligations of your pact or serve joyfully in anticipation of the rewards promised to you? Your patron's demands might drive you into adventures, or they might consist entirely of small favors you can do between adventures.

## Gallery
<div class="gallery">

![Warlock 1](img/classes/warlock_1.png)
![Warlock 2](img/classes/warlock_2.jpg)
![Warlock 3](img/classes/warlock_3.png)
![Warlock 4](img/classes/warlock_4.jpg)
![Warlock 5](img/classes/warlock_5.png)
![Warlock 6](img/classes/warlock_6.jpg)

</div>
