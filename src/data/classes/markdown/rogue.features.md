## Sneak Attack
At 1st level, you know how to strike subtly and exploit a foe's distraction. Once per turn, you can deal extra damage to one creature you hit with a weapon attack using Dexterity if you have advantage on the attack roll. The amount of extra damage you deal is determined by the your levels in this class, as shown in the Sneak Attack column of the Rogue table

## Cunning Action
At 1st level, your quick thinking and agility allow you to move and act quickly. You can take the Dash, Disengage or Hide action as a bonus action on each of your turns.

## Rogue Archetype
At 1st level, you choose an archetype that you emulate in the exercise of your rogue abilities. You either become an assassin, a thief, or a scout. The archetype you choose grants you features at 1st level and again at 3rd, 7th, 11th, 15th and 19th level.

<div class="assassin">
<blockquote>

## Trained Killer
At 1st level, you focus your training on the grim art of death. Stealth, poison, and disguise help you eliminate your foes with deadly efficiency. You gain proficiency with the dusguise’s tools and poisoner’s tools.

</blockquote>
</div>

<div class="thief">
<blockquote>

## Burglar
At 1st level, you hone your skills in the larcenous arts. In addition to improving your agility and stealth, you learn skills useful in dangerous situations. You gain proficiency with the Dexterity (Sleight of Hand) skill. 
Additionally, you can take the Disarm and Steal action as a bonus action on your turn, as part of your Cunning Action feature. 
	
</blockquote>
</div>

<div class="scout">
<blockquote>

## Skirmisher
At 1st level, you are skilled in stealth and surviving far from the streets of a city, allowing you to scout ahead of your companions during expeditions. You gain proficiency in the Wisdom (Survival) skill. 
Additionally, whenever you take the Attack action on your turn, you can move up to half your speed as part of that action. This movement doesn’t provoke opportunity attacks.
	
</blockquote>
</div>

## Expertise
At 2nd level, you are specifically trained in a way to support your independent actions as a rogue. Choose three of your skill or tool proficiencies. Your proficiency bonus is doubled for any ability check you make that uses either of the chosen proficiencies. 

## Uncanny Dodge
At 2nd level, by training your reflexes, you learn to lessen the impact of attacks targeting you. When you take damage from a source that you can see, you can use your reaction to halve the damage.

<div class="assassin">
<blockquote>

## Assassinate
At 3rd level, you are at your deadliest when you get the drop on your enemies. You have advantage on attack rolls against any creature that hasn't taken a turn in combat yet. 
Additionally, any hit you score against a creature that is surprised is a critical hit.

</blockquote>
</div>

<div class="thief">
<blockquote>

## Dirty Fighting
At 3rd level, you learn how to employ a variety of underhanded moves to gain the upper hand in a fight. Whenever you deal damage to a creature with your Sneak Attack feature, you can impose disadvantage on that creature’s attack rolls against you until the end of its next turn. 

</blockquote>
</div>

<div class="scout">
<blockquote>

## Tactical Insight
At 3rd level, you can use your improved understanding of your surroundings to support your allies. You can use the Help action as a bonus action. 
Additionally, when you use the Help action to aid an ally in attacking a creature, the target of that attack can be within 30 feet of you, rather than within 5 feet of you, if the target can see or hear you.
	
</blockquote>
</div>

## Ability Score Improvement
When you reach 4th level, and again at 6th, 8th, 12th, 14th, and 16th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1, or you can take a feat for which you fulfill the requirements. As normal, you cannot increase an ability score above 20 using this feature.

## Extra Attack
At 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.

<div class="assassin">
<blockquote>

## Infiltrator
At 7th level, you can unfailingly create false identities for yourself. During a long rest, you can establish the history, profession, and affiliations for an identity. If you choose to adopt a new identity, other creatures believe you to be that person until given an obvious reason not to. 
Additionally, if you observe a humanoid for at least 1 hour, you can adopt that person's speech, writing, and behavior.

</blockquote>
</div>

<div class="thief">
<blockquote>

## Supreme Mobility
At 7th level, your experience in your line of work has given you an edge when maneuvering in certain situations. Climbing no longer costs you extra movement and the distance you cover when you make a running jump is doubled. 
Additionally, when you roll for initative, you can add your proficiency bonus to the total.  
	
</blockquote>
</div>

<div class="scout">
<blockquote>

## Flurry
At 7th level, you have the ability to ensure that your attacks count. Your attacks can benefit from your Sneak Attack feature even if you have already used it this turn, aslong as it targets a different creature. 

</blockquote>
</div>

## Eye for Weakness
At 9th level,  you gain the ability to spot a weakness in a targets defenses. When you hit a creature with a weapon attack using Dexterity, you can choose to use your Sneak Attack feature against that creature even if you don’t have advantage on the attack roll.
Once you use this feature, you can’t use it again until you finish a short or long rest. 
You gain an additional use of this feature when you reach 17th level in this class. 


## Evasion
At 10th level, you can nimbly dodge out of the way of certain area effects. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.

<div class="assassin">
<blockquote>

## Coup de Grâce
At 11th level, you learn to effectively extinguish the life from your targets. Whenever you deal damage to a creature with your Sneak Attack feature and the creature’s hit point are reduced to an amount equal to or below double your Rogue level, it is instead reduced to 0 hit points and killed outright.

</blockquote>
</div>

<div class="thief">
<blockquote>

## Supreme Sneak
At 11th level, you perfect your talent to move stealthily. You have advantage on Dexterity (Stealth) checks if you move no more than half your speed on the same turn. 

</blockquote>
</div>

<div class="scout">
<blockquote>

## Defensive Tactics
At 11th level, you learn techniques that help you defend against the threats you face. After a creature you can see deals damage to you, you gain resistance to all damage done to you by that creature for the rest of it’s turn.
	
</blockquote>
</div>

## Reliable Talent
At 13th level, you have refined your chosen skills until they approach perfection. Whenever you make an ability check that lets you add your proficiency bonus, you can treat a d20 roll of 9 or lower as a 10.

<div class="assassin">
<blockquote>

## Hide in Plain Sight
At 15th level, you learn to camouflage yourself effectively. 
You gain a +5 bonus to Dexterity (Stealth) checks as long as you remain still, without moving or taking actions. Once you move or take an action or a reaction, you loose this benefit.

</blockquote>
</div>

<div class="thief">
<blockquote>

## Multitasker
At 15th level, you become accustomed to quick decision making and learn to adopt on the fly. You gain an additional use of your bonus action on each of your turns. This bonus action can only be used by your Cunning Action feature. 
	
</blockquote>
</div>

<div class="scout">
<blockquote>

## Stalker
At 15th level, you master the art of the ambush. On your first turn during combat, you gain a +10 bonus to your speed and if you take the Attack action on that turn, you can make on additional attack as part of that action. 
	
</blockquote>
</div>

## Elusive
At 18th level, you are so evasive that attackers rarely gain the upper hand against you. No attack roll has advantage against you while you aren't incapacitated.

<div class="assassin">
<blockquote>

## Masterful Strikes
At 19th level, the accuracy and lethality of your attacks reaches perfection. Change the damage die of your Sneak Attack feature from d6 to d8.

</blockquote>
</div>

<div class="thief">
<blockquote>

## Stroke of Luck
At 19th level, you have an uncanny knack for succeeding when you need to. If you roll an ability check, you can instead treat the d20 roll as a 20. You can do so after you roll the die, but before the outcome is determined.
Once you use this feature, you can't use it again until you finish a short or long rest.
	
</blockquote>
</div>

<div class="scout">
<blockquote>

## Subtle Mastery
At 19th level, your uncanny speed and mastery of improvisation has given you an edge in battle. You can take two turns during the first round of any combat. You take your first turn at your normal initiative and your second turn at your initiative minus 10. You can't use this feature when you are surprised.
	
</blockquote>
</div>

## Most Wanted
At 20th level, your honed abilites and dangerous presence is feared throughout the land. At the end of a round of combat, after each creature in the turn order has acted, you can choose to take one additional turn. 
Once you use this feature, you can't use it again until you finish a short or long rest.