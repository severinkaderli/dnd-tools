## Hit Points
**Hit Dice:** 1d10 per Fighter level  
**Hit Points at 1st level:** 10 + your Constitution modifier  
**Hit Points at higher levels:** 1d10 (or 5) + your Constitution modifier per Fighter level after the 1st

## Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Strength and Dexterity  
**Armor:** light armor, medium armor, heavy armor, shields  
**Weapons:** simple weapons, martial weapons  
**Tools:** leatherworker's tools or smith's tools  
**Languages:** none  
**Skills:** Athletics and two skills of your choice  

## Starting Equipment
You start with the following items, plus anything provided by your background:

* a chain shirt or chain mail   
* a martial weapon and a shield or two martial weapons  
* a crossbow or a longbow  
* a dungeoneer’s pack or an explorer’s pack  
