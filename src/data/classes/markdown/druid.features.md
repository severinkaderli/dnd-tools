## Spellcasting
At 1st level, Drawing on the essence of nature itself, you are able to cast spells at will.

### Spells known
You know three 1st-level spells of your choice from the Druid spell list. The Spells Known column of the Druid table shows when you learn more spells of your choice. Each of these spells must be of a level for which you have spell slots, as shown on the table. For instance, when you reach 3rd level in this class, you can learn new spells of 1st or 2nd level.

### Cantrips
At 1st level, you know two cantrips of your choice from the Druid spell list. You learn additional Druid cantrips of your choice at higher levels, as shown in the Cantrips column of the Druid table.

### Spellcasting Ability
Wisdom is your spellcasting ability for your Druid spells. In addition, you use your Wisdom modifier when setting the saving throw DC for a Druid spell you cast and when making an attack roll with one.

**Spell save DC:** 8 + your proficiency bonus + your Wisdom modifier  
**Spell attack:** your proficiency bonus + your Wisdom modifier

### Spellcasting Focus
You can use a druidic focus for casting your Druid spells.

## Ritual Casting
At 1st level, you learn the unique art of entreating nature to help cast your spells. When you cast a Druid spell of 1st level or higher, you can choose to instead cast it as a ritual. When you choose to do so, it takes additional time. For each level of the spell, the casting time is increased by 10 minutes. The spell is then cast at its lowest level but doesn’t expend a spell slot. 
After you use this feature a number of times equal to your Wisdom modifier, you must finish a long rest before you can use it again. 

## Druidic Circle
At 1st level, you choose to identify with a circle of druids, shaping your practice as a Druid. Your choice grants you features when you choose it at 1st level and again at 2nd, 6th, 10th, 14th, and 18th level.

<div class="circle-of-the-wild">
<blockquote>

## Believer of the Old Faith
At 1st level, you become a member of a group of fierce guardians of the wilds, who safeguard ancient knowledge and practice a vast oral tradition. You gain proficiency in the Survival skill.  
You also gain access to the following circle spells:

### Circle Spells
You gain access to certain spells connected to your druidic circle, called circle spells. These spells always count as Druid spells for you. Once you gain access to a circle spell, you add it to your list of spells known and it does not count against the number of spells you can learn.

| Spell Level | Circle Spells |
|-----|---------------------|
| 1st | _[Keen Senses](spells#keen-senses)_ |
| 2nd | _[Bestial Charge](spells#bestial-charge)_ |
| 3rd | _[Apsect Tattoo](spells#aspect-tattoo)_ |
| 4th | _[Splitting Shout](spells#splitting-shout)_ |
| 5th | _[Aspect of Hydra](spells#aspect-of-hydra)_ |

</blockquote>
</div>

<div class="circle-of-the-fey">
<blockquote>

## Feytouched
At 1st level, you seek to travel the world, protecting the realms with your magic and merry teaching. You learn to speak, read, and write Sylvan.  
You also gain access to the following circle spells:

### Circle Spells
You gain access to certain spells connected to your druidic circle, called circle spells. These spells always count as Druid spells for you. Once you gain access to a circle spell, you add it to your list of spells known and it does not count against the number of spells you can learn.
 
| Spell Level | Circle Spells |
|-----|---------------------|
| 1st | _[Faerie Fire](spells#faerie-fire)_ |
| 2nd | _[Fey Presence](spells#fey-presence)_ |
| 3rd | _[Misty Escape](spells#misty-escape)_ |
| 4th | _[Lifehunt Scythe](spells#lifehunt-scythe)_ |
| 5th | _[Dark Delirium](spells#dark-delirium)_ |

</blockquote>
</div>

<div class="circle-of-the-north">
<blockquote>

## Northener
At 1st level, you have become accustomed to winter’s harsh climates. You gain resistance to cold damage and you are immune against the negative effects of cold temperature.
You also gain access to the following circle spells:

### Circle Spells
You gain access to certain spells connected to your druidic circle, called circle spells. These spells always count as Druid spells for you. Once you gain access to a circle spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 
 
| Spell Level | Circle Spells |
|-----|---------------------|
| 1st | _[Ice Tomb](spells#ice-tomb)_ |
| 2nd | _[Melt](spells#melt)_ |
| 3rd | _[Frozen Razors](spells#frozen-razors)_ |
| 4th | _[Iceform](spells#iceform)_ |
| 5th | _[Fimbulvinter](spells#fimbulvinter)_ |

</blockquote>
</div>

## Healing Surge
At 2nd level, you learn to call the mending power of nature at you side. Whenever you cast a Druid spell of 1st level or higher, you regain hit points equal to your Wisdom modifier.

<div class="circle-of-the-wild">
<blockquote>

## Wild Shape
At 2nd level, you can use your bonus action to magically assume the shape of a beast that you have seen before, with a challenge rating as high as half your Druid level (rounded down). 
You can use this feature a number of times equal to your Wisdom modifier. Assuming the shape of a large beast expends two uses of this feature, and assuming the shape of a huge beast or larger expends three uses. You regain expended uses of your Wild Shape feature when you finish a long rest.  
You can stay in beast shape for a number of hours equal to half your druid level (rounded down). You then revert to your normal form unless you expend another use of this feature to extend the duration. You can revert to your normal form earlier by using a bonus action on your turn. You automatically revert if you fall unconscious, drop to 0 hit points, or die.
While you are transformed, the following rules apply:  

* Your game statistics are replaced by the statistics of the beast, but you retain your Intelligence, Wisdom, and Charisma scores. You also retain all of your skill and saving throw proficiencies, in addition to gaining those of the creature. If the creature has the same proficiency as you and the bonus in its stat block is higher than yours, use the creature’s bonus instead of yours. If the creature has any legendary or lair actions, you can’t use them.  

* When you transform, you assume the beast’s hit points. When you revert to your normal form, you return to the number of hit points you had before you transformed. However, if you revert as a result of dropping to 0 hit points, you are stunned until the end of your next turn and any excess damage carries over to your normal form.  

* You can’t cast spells, and your ability to speak or take any action that requires hands is limited to the capabilities of your beast form. Transforming doesn’t break your concentration on a spell you’ve already cast or prevent you from taking actions that are part of an ongoing spell.  

* Your equipment merges into your new form and has no effect until you leave the form.  

</blockquote>
</div>

<div class="circle-of-the-fey">
<blockquote>

## Balm of the Spring Forum
At 2nd level, you become a font of energy that lends relief to weary feet and respite from injuries. Whenever you regain hit points from your Healing Surge feature or from a Druid spell that you cast, you can choose a creature within 30 feet of you that you can see, that creature regains the same amount of hit points. 

</blockquote>
</div>

<div class="circle-of-the-north">
<blockquote>
	
## Biting Frost
At 2nd level, your connection to the primordial forces of winter strengthens your understanding of certain spells. Whenever a Druid spell cast by you deals cold damage to a creature that you can see, you can use your bonus action on the same turn to summon biting frost to freeze the target. Until the end of it’s next turn, the creature’s speed is reduced by an amount of feet equal to your Wisdom modifier x 5. If a creature’s speed is reduced to 0 by this feature, it becomes grappled for 1 minute or until it uses its action to attempt a Strength check against your spell save DC to break free.

</blockquote>
</div>

## Nature's Envoy
At 3rd level, creatures of the wild sense your connection to nature and become hesitant to attack you. When a beast attacks you, that creature must make a Wisdom saving throw against your Druid spell save DC. On a failed save, the creature must choose a different target, or the attack automatically misses. On a successful save, the creature is immune to this effect for 24 hours.  
The creature is aware of this effect before it makes its attack against you.

## Ability Score Improvement
When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two Ability Scores of your choice by 1, or you can take a feat for which you fulfil the requirements. As normal, you cannot increase an ability score above 20 using this feature.

<div class="circle-of-the-wild">
<blockquote>

## Primal Strike
At 6th level, your attacks while in wild shape count as magical for the purpose of overcoming resistance and immunity to nonmagical attacks. 
Additionally, once per turn, while you are transformed, when you hit a creature with an attack, you may expend one spell slot to deal additional damage to the target. The extra damage is 2d6 for a 1st-level spell slot, plus 1d6 for each spell level higher than 1st.

</blockquote>
</div>

<div class="circle-of-the-fey">
<blockquote>

## Retort of the Summer Court
At 6th level, you learn how to turn the mind-affecting magic of your enemies against them. You are immune against being charmed. Additionally, when another creature targets you with an enchantment spell, you can use your reaction to attempt to turn the spell back on that creature. The target must succeed on a Wisdom saving throw against your Druid spell save DC or suffer the consequences of the spell it tried to cast. You decide how that spell affects the creature.  

</blockquote>
</div>

<div class="circle-of-the-north">
<blockquote>
	
## Song of Rime
At 6th level, you learn to transform excess energy into a protective shield. Whenever you regain hit points from your Healing Surge feature or from a Druid spell that you cast, you can choose to gain the same amount of temporary hit points. While you have these hit points, you gain a +2 bonus to your AC and you have resistance to non-magical bludgeoning, piercing, and slashing damage.

</blockquote>
</div>

## Timeless Body
At 10th level,  the primal magic that you wield causes you to age more slowly. For every 10 years that pass, your body ages only 1 year.
Additionally, you are immune against effects or spells that change or alter your age.

<div class="circle-of-the-wild">
<blockquote>

## Improved Wild Shape
At 10th level, you learn to further enhance your shaping. When using your Wild Shape feature, your beast form gains the following benefits:

* You add your Wisdom modifier to attack and damage rolls that you make.  
* Your maximum hit points increase by an amount equal to twice your Druid level.  

</blockquote>
</div>

<div class="circle-of-the-fey">
<blockquote>

## Paths of the Autumn Drove
At 10th level, you can use hidden, unpredictable magical pathways to traverse space in a blink of an eye. Whenever you cast a Druid spell of 1st level or higher, you can teleport up to 30 feet to an unoccupied space you can see. Alternatively, you can choose a space within range that is occupied by an allied creature. If that creature is willing, you both teleport, swapping places.

</blockquote>
</div>

<div class="circle-of-the-north">
<blockquote>

## Summon Elemental
At 10th level, you can call upon nature and summon forth a creature of pure ice. As an action on your turn, you summon the creature in an unoccupied space within 5 feet of you. You choose its appearance, but it uses the earth elemental’s statistics, with the following changes:

* The creature loses its burrow speed and it’s Earth Glide ability.  
* The creature gains resistance to cold damage and vulnerability to fire damage.  
* The creature’s hit point maximum is equal to your hit point maximum.   

Roll initiative for the creature. You control its actions on its turn. The creature disappears when it drops to 0 hit points, when you drop to 0 hit points, or when 1 hour is passed.  
Once you use this feature, you can’t use it again until you finished a long rest.

</blockquote>
</div>

<div class="circle-of-the-wild">
<blockquote>

## Beast Spells
At 14th level, you learn to cast your Druid spells while being transformed using your Wild Shape feature. Additionally, you can ignore the material component of Druid spells that you cast while using your Wild Shape feature. 

</blockquote>
</div>

<div class="circle-of-the-fey">
<blockquote>

## Wrath of the Winter Tribunal
At 14th level, whenever you cast a Druid spell that targets one or more creatures, you can choose to additionally end any spells that currently affect the target, as per the *dispel magic* spell. If the spell targets more than one creature, you must choose to either affect none or all of the targets with this feature.

</blockquote>
</div>

<div class="circle-of-the-north">
<blockquote>

## Mastery of Ice
At 14th level, your control over the elemental power of ice has become extremely potent and augmented your features. You gain the following benefits:

* A creature grappled by your Biting Frost feature gains vulnerability to cold damage.  
* If a creature hits you with a melee attack while you have temporary hit points gained by your Song of Rime feature, the creature takes cold damage equal to your Druid level.

</blockquote>
</div>

<div class="circle-of-the-wild">
<blockquote>

## Elder of the Pack
At 18th level, your shaping has become extremely potent. When you use your Wild Shape feature, your beast form gains the following benefits:  

* Increase the damage die of your primal strike feature by one (d8 instead of d6).   
* You are no longer stunned when reverting as a result of dropping to 0 hit points.  
 
</blockquote>
</div>

<div class="circle-of-the-fey">
<blockquote>

## Consort of the Fey
At 18th level, your mortal form mingles with the essence of the feywild, granting you powers analogous to the archfey. You always count as a fey for all spells and abilities that affect them and have advantage on all Charisma checks when interacting with fey creatures.  
Additionally, creatures with a CR lower than your Druid level have disadvantage on their saving throws against Druid spells that you cast. 

</blockquote>
</div>

<div class="circle-of-the-north">
<blockquote>

## Essence of Winter
At 18th level, you manage to infuse all your druidic spells with the primordial power of winter, ice and snow. When you cast a Druid spell of 1st-level or higher that deals damage, you can change the damage type of that spell to cold damage.  
Additionally, creatures without resistance or immunity against cold damage have disadvantage on their saving throws against Druid spells that you cast.

</blockquote>
</div>

## Archdruid
At 20th level, you can inquire the help of the primeval power of nature itself when your need is great. Imploring nature’s aid requires you to use your action. Describe the assistance you seek. The DM chooses the nature of the assistance.
Once you use this feature, you cannot use it again for 7 days.

