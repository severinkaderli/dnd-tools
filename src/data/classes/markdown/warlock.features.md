## Pact Weapon
At 1st level, the influence of your patron allows you to mystically channel your will through a particular weapon. You can use your bonus action to create a pact weapon in your empty hand. It takes the form of a simple or martial weapon and you can choose the form that it takes each time you create it. You are proficient with it while you wield it. This weapon counts as magical for the purpose of overcoming resistance and immunity to nonmagical attacks.  
You can dismiss the weapon as a bonus action. Otherwise, it disappears if you use this feature again, or if you die. 

## Spellcasting
At 1st level, your research into the occult and the magic bestowed on you by your patron has given you facility with spells.

### Spells known
You know two 1st-level spells of your choice from the Warlock spell list. The Spells Known column of the Warlock table shows when you learn more spells of your choice. Each of these spells must be of a level for which you have spell slots, as shown on the table. For instance, when you reach 5th level in this class, you can learn new spells of 1st or 2nd level.

### Spellcasting Ability
Intelligence is your spellcasting ability for your Warlock spells. In addition, you use your Intelligence modifier when setting the saving throw DC for a Warlock spell you cast and when making an attack roll with one.

**Spell save DC: 8** + your proficiency bonus + your Intelligence modifier  
**Spell attack:** your proficiency bonus + your Intelligence modifier

### Spellcasting Focus
You can use your pact weapon or an arcane focus for casting your Warlock spells.

## Warlock Pact
At 1st level, you have struck a bargain with a powerful otherworldly being of your choice: An Infernal from the lower planes, a being from the Shadowfell or an inconceivable Old One. Your choice grants you features when you choose it at 1st level and again at 3rd, 7th, 11th, 15th, and 19th level.

<div class="infernal-patron">
<blockquote>

## Dark Arts
At 1st level, you have made a pact with a being from the lower planes of existence, a being whose goal is manifold but ultimately includes you. You learn to speak read and write Infernal.    
You also gain access to the following pact spells:

### Pact Spells
You gain access to certain spells connected to your patron, called pact spells. These spells always count as Warlock spells for you. Once you gain access to a pact spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Pact Spells |
|-----|---------------------|
| 1st | _[Hellish Rebuke](spells#hellish-rebuke)_ |
| 2nd | _[Dark Secret](spells#dark-secret)_ |
| 3rd | _[Blackened Heart](spells#blackened-heart)_ |
| 4th | _[Shadows of Turmoil](spells#shadows-of-turmoil)_ |
| 5th | _[Hurl Through Hell](spells#hurl-trough-hell)_ |

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

## Make Contact
At 1st level, your patron is a mysterious entity, with knowledge so immense and ancient that even the greatest libraries pale in comparison. You gain proficiency in the History skill.    
You also gain access to the following pact spells:

### Pact Spells
You gain access to certain spells connected to your patron, called pact spells. These spells always count as Warlock spells for you. Once you gain access to a pact spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Pact Spells |
|-----|---------------------|
| 1st | _[Dissonant Whispers](spells#dissonant-whispers)_ |
| 2nd | _[Curse of Oblivion](spells#curse-of-oblivion)_ |
| 3rd | _[Luring Light](spells#luring-light)_ |
| 4th | _[Black Tentacles](spells#black-tentacles)_ |
| 5th | _[Falling Star](spells#falling-star)_ |

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

## Wicked Succour
At 1st level, as an instrument of a being that rules within the dread realm, you personify its will and determination. You can see in darkness as if it were dim light.    
You also gain access to the following pact spells:

### Pact Spells
You gain access to certain spells connected to your patron, called pact spells. These spells always count as Warlock spells for you. Once you gain access to a pact spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Pact Spells |
|-----|---------------------|
| 1st | _[Duskwalk](spells#duskwalk)_ |
| 2nd | _[Shadow Armor](spells#Onslaught)_ |
| 3rd | _[Fell Onslaught](spells#fell-onslaught)_ |
| 4th | _[Unseen Claw](spells#unseen-claw)_ |
| 5th | _[Negative Energy Flood](spells#negative-energy-flood)_ |

</blockquote>
</div>

## Fighting Style
At 2nd level, you adopt a particular style of fighting as your speciality. Choose one Fighting Style. 

<details>
<summary>Fighting Styles</summary>

### Defensive Style
While you are wearing armor, you gain a +1 bonus to AC.

### Dueling Style
When you are wielding a melee weapon in one hand and no other weapons, you gain a +2 bonus to damage rolls with that weapon.

### Great Weapon Style
When you roll a 1 on a damage die for an attack you make with a weapon that has the two-handed property, you can reroll the die and must use the new roll.

### Protective Style
When a creature you can see attacks a target other than you that is within 5 feet of you, you can use your reaction to impose disadvantage on the attack roll. 

### Ranged Weapon Style
You gain a +2 bonus to attack rolls you make with ranged weapons.

### Two-Weapon Style
You can engange in two-weapon fighting even when one of the two melee weapons you are wielding doesn't have the light property.

</details>

## Invocations
At 2nd level, your understanding of occult lore has given you the possibility to unearth eldritch invocations, fragments of forbidden knowledge that imbue you with magical abilities. You gain an eldritch invocation of your choice.   
When you gain certain Warlock levels, you gain additional invocations of your choice, as shown in the Invocations Known column of the Warlock table.

<details>
<summary>Invocations</summary>

### Book of Solomon
You gain a glimpse into ancient Salomon’s’ only written work, containing eldritch truths beyond imagination. It allows you to choose two spells from the sorcerer and/or wizard spell list and add them to your spells known. The spells you choose must be of a level you can cast and do not count against the number of spells you can learn.   

### Grimoire of Ayporos
Ayporos was a prodigious scholar, who’s curiosity lead him down a path of planar discovery. Researching his legendary deeds grants you powerful knowledge related to ancient magic. You can add double your proficiency modifier to any Arcana check you make, and you have advantage on any Arcana check related to the planes of existence, demiplanes and otherwise artificially created planes.  
Additionally, once you reach 18th level in this class, you can cast _Planenshift_ using your Mystic Arcanum. 

### Hymn of Tourach
You begin to hear the first few notes of exiled Tourach’s last song. Its terrible nature grants you otherworldly vigour. You no longer need to sleep and can't be forced to sleep by any means. To gain the benefits of a long rest, you can spend all 8 hours doing light activity, such as reading, crafting or keeping watch.  
Additionally, you gain proficiency in one instrument of your choice and the Performance skill. If you are already proficient in the Performance skill, you instead gain proficiency in one additional instrument of your choice. 

### Incantations of Carasphyx
You uncover pieces of forbidden knowledge, left behind by Carasphyx the many-faced. This allows you to alter your appearance at will. You add _disguise self_ to your spells known, it does not count against the number of spells you can learn, and you can cast it at will, without expending a spell slot.  
Additionally, creatures trying to see through your disguise self spell have disadvantage on their Investigation check. 

### Memoir of Buné
Infamous Buné was feared as a calamity. This accursed document grants you insight into his dreadful life and his eldritch knowledge. You can add your Intelligence modifier to the damage you deal with warlock spells you cast. 

### Runes of Caryll
Secret symbols left by Caryll, the first warlock. Those branded by it enjoy a fraction of his occult power. When you hit a creature with your pact weapon, you deal additional damage equal to your Intelligence modifier.

### Secrets of Khirad
Powerful, eldritch secrets uncovered by lost Khirad still echo through places best left unchecked. Knowledge of these secrets strengthens your control over your spells. You have advantage on Constitution saving throws that you make to maintain concentration on a spell when you take damage.   
Additionally, you can concentrate on two spells simultaneously if both are cast on their lowest level.  

### Sign of Cassilda
You manage to memorize a forbidden sign, conceived in secret by ancient Cassilda, that clouds one’s soul. You are considered to be always under the effect of the _nondetection_ spell.  
Additionally, once you reach 9th level in this class, you learn to imprint this sign onto others. At the end of each long rest, you can choose a number of willing creatures that you can see equal to your Intelligence modifier. Each chosen creature also benefits from the effects of the _nondetection_ spell until you choose different creatures.

### Teachings of Raym
Raym was an ambitious scholar, an expert in uncovering the unknown, who hid his findings across the planes. Knowledge of his work allows you to read all writing, no matter the language.  
Additionally, you add _detect magic_ to your spells known, it does not count against the number of spells you can learn, and you can cast it at will, without expending a spell slot.

### Whispers of Ulban
You recover fragments of king Ulban’s utterings, lost secrets describing the duality of mortality. Understanding these whispers increases your hit point maximum by an amount equal to your level. Whenever you gain a level thereafter, your hit point maximum increases by an additional hit point.   
Additionally, once you reach 9th level in this class, you add _speak with dead_ to your spells know, it does not count against the number of spells you can learn and you can cast it at will, without expending a spell slot.

<div class="infernal-patron">
<blockquote>

### Hellfire of Mephistopheles 
You gain access to one of The Lord of No Mercy’s own designs, powerful hellfire that coats your weapon and burns your foes with no chance of escape. You change the damage type of your pact weapon to fire and attacks made with your pact weapon ignore resistance and immunity to fire damage.   
Additionally, when you roll a 1 on a damage die for an attack made with your pact weapon, you can reroll the die and must use the new roll.

### Inquisition of Levistus    
The Prince of Stygia has taken an interest in your actions and nudges their outcome in seemingly favourable ways. You gain proficiency in one saving throw of your choice.   
Additionally, once your reach 7th level in this class, you can use your Infernal Luck feature one additional time before you have to finish a short or long rest to use it again. 

### Orders of Asmodeus  
In ultimate beneficence, the Ruler of the Nine Hells has granted you the ability to imbue your voice with a fraction of his authority. You gain proficiency in the Intimidation skill and you have advantage on any Charisma check when interacting with infernal creatures.   
Additionally, once you reach 15th level in this class, you gain advantage on your Intelligence check when maintaining your servant’s loyalty of your Summon Servant feature.  

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

### Form of Hadar  
Blessed to make contact with the Dark Hunger, you learn eldritch truths that unlock your mortal potential. You always count as an aberration for all spells and abilities that affect them. You do no longer need air, nourishment, water or light to live, but you can still ingest food and drink if you wish. You are immune against the negative effects of extreme temperature. You also age more slowly. For every 10 years that pass, your body ages only 1 year and your age can’t change through magical means.  
Additionally, you are immune against diseases and the poisoned condition. 

### Nightmare of Caiphon 
Through painful visions of the Dream Whisperer, your understanding of the minds of mortals deepens. You add _detect thoughts_ to your spells known, it does not count against the number of spells you can learn, and you can cast it at will, without expending a spell slot.  
Additionally, once you reach 11th level in this class, you learn to create an unlimited amount of Thralls for your Create Thrall feature.

### Scales of Dendar
Remnants of the Night Serpent, left behind in forgotten places, find their way to you. They bolster your material and immaterial defences. You gain a +2 bonus to your AC.  
Additionally, once your reach 7th level in this class, you can use your Entropic Ward feature one additional time before you have to finish a short or long rest to use it again.

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

### Ambitions of Vecna  
The Whispered One’s desires are manifold, and he discovers you as a worthy tool in his ever-present conquests. When you are reduced to 0 hit points, you can use your reaction to drop to 1 hit point and gain one level of exhaustion instead.  

### Beauty of Lloth 
You gain a shard of the Queen of Spiders’ alien beauty, causing other creatures to become enraptured by your appearance. You gain proficiency in the Persuasion skill and you have advantage on any Persuasion check when interacting with humanoid creatures.

### Cloak of the Raven Queen  
The patron of death grants you her boon, basking you in shadow, increasing your defences and help you stay unseen. You add _mage armor_ to your spells known, it does not count against the number of spells you can learn, and you can cast it at will, without expending a spell slot.    
Additionally, you have advantage on stealth checks while under the effect of _mage armor_.

</blockquote>
</div>	

</details>

<div class="infernal-patron">
<blockquote>

## Lifedrinker
At 3rd level, whenever a hostile creature is reduced to 0 hit points within 15 feet of you, you can use your reaction to regain hit points equal to your Warlock level.

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

## Awakened Mind
At 3rd level, your alien knowledge gives you the ability to touch the minds of other creatures. You can communicate telepathically with any creature you can see within 60 feet of you. You do not need to share a language with the creature for it to understand your telepathic utterances, but the creature must be able to understand at least one language.

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

## Ethereal Strikes
At 3rd level, you combine your martial training with your dark magic and cause your pact weapon to emit ghostly flames. Your attacks with this weapon ignore cover and creatures hit by it have their speed reduced by 10 feet until the end of their next turn

</blockquote>
</div>

## Ability Score Improvement
When you reach 4th level, and again at 6th, 8th, 12th, 14th, and 16th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1, or you can take a feat for which you fulfill the requirements. As normal, you cannot increase an ability score above 20 using this feature.

## Extra Attack
At 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.

<div class="infernal-patron">
<blockquote>

## Infernal Luck
At 7th level, you can call on your patron to alter fate in your favour. Whenever you make an attack roll, an ability check, or a saving throw, you can choose to roll an additional d20, choosing either result.  
Once you use this feature, you cannot use it again until you finish a short or long rest.

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

## Entropic Ward
At 7th level, you learn to magically ward yourself against attacks. Whenever you take damage, you can use your reaction to become immune to a damage type of your choice until the end of this turn.  
Once you use this feature, you can’t use it again until you finished a short or long rest. 

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

## Beast of Ill Omen
At 7th level, you can call upon your patron and summon forth a creature of darkness. As an action on your turn, you summon the creature in an unoccupied space within 5 feet of you. You choose it’s appearance, but it uses the dire wolf’s statistics, with the following changes:  

* The creature’s size is Medium and it counts as a monstrosity.  
* It’s hit point maximum is equal to your hit point maximum.   
* It gains a bonus to its AC and it’s attack and damage rolls equal to your Intelligence modifier.   
* It can attack twice, instead of once, whenever it takes the Attack action on it’s turn.  

Roll initiative for the creature. You control its actions on its turn. The creature disappears when it drops to 0 hit points, when you drop to 0 hit points, or when 1 hour is passed.  
Once you use this feature, you can’t use it again until you finished a long rest. 

</blockquote>
</div>

## Hexblade
At 10th level, you learn to transform one magic weapon in your possession into a pact weapon by performing a ritual over the course of a long rest. You can summon and dismiss the weapon as a bonus action, shunting it into an extradimensional space when not in use.  
The weapon ceases being your pact weapon if you die, if you perform the ritual on a different weapon, or if you use the ritual to break your bond to it.

<div class="infernal-patron">
<blockquote>

## Devil's Blessing
At 11th level, when you use your action to cast a spell, you can make one weapon attack as a bonus action.

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

## Create Thrall
At 11th level, you gain the ability to infect a humanoid’s mind with the alien magic of your patron. You can use your action to touch an incapacitated humanoid. That creature is then charmed by you until a remove curse spell of high enough level is cast on it. You can communicate telepathically with the charmed creature as long as the two of you are on the same plane of existence. Although the creature isn’t under your control, it takes your requests or actions in the most favourable way it can.  
You can have a number of Thralls equal to your Intelligence modifier.

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

## Supernatural Condition
At 11th level, your connection to the shadowfell has altered your physique, granting you unnatural vigour. You gain resistance to nonmagical bludgeoning, piercing and slashing damage.  
Additionally, when you start your turn in an area of dim light or darkness, you can choose to become invisible. You remain invisible until you make an attack, cast a spell, or are in an area of bright light. 

</blockquote>
</div>

<div class="infernal-patron">
<blockquote>

## Summon Servant
At 15th level, you can use your action to summon a hellish servant to aid you in battle. You summon an infernal creature of the DMs choice with a maximum challenge rating equal to half of your Warlock level (round down) within an unoccupied space within 30 feet of you that you can see.   
Roll initiative for the servant. You control it’s actions as long as it remains loyal to you. At the end of each of its turns, you must succeed on an Intelligence check equal to 8 + the challenge rating of your servant. If you fail, it loses it’s loyalty to you and starts acting on its own, pursuing and attacking the nearest non-infernal creatures that it can see. The servant disappears when it drops to 0 hit points, when you drop to 0 hit points, or when 1 hour is passed.  
Once you use this feature, you cannot use it again until you finish a long rest.

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

## Eldritch Strike
At 15th level, you learn how to make your weapon strikes undercut a creature's resistance to your spells. When you hit a creature with a weapon attack, that creature has disadvantage on the next saving throw it makes against a spell you cast before the end of your next turn.

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

## Shadowkeeper
At 15th level, you learn to seize the latent power of a parting soul to bolster your strength. Whenever a hostile creature is reduced to 0 hit points within 15 feet of you, you can use your reaction to temporarily capture it’s soul. For each captured soul, you gain a +1 bonus to your attack and damage rolls with your pact weapon. Your summoned creature from you Beast of Ill Omen feature gains the same benefit on it’s bite attack. Captured souls disperse from your hold after 24 hours. 

</blockquote>
</div>

## Mystic Arcanum
At 18th level, your patron bestows upon you a magical secret called an arcanum. When you cast a Warlock spell of 1st level or higher you can choose to use your arcanum to cast that spell at 9th level instead.  
You must finish a long rest before you can do so again.

<div class="infernal-patron">
<blockquote>

## Demon Form
At 19th level, you unlock the power to incarnate in the form of your patron. As an action, you wrap yourself hellish flame and transform into an infernal creature similar to your patron’s appearance. For 10 minutes, or until you exit this incarnation as a bonus action, you gain the following benefits:

* Your size becomes Large.  
* You gain temporary hit points equal to twice your Warlock level.   
* You gain a flying speed of 50 feet.   
* You can attack three times whenever you take the Attack action on your turn.  
* You are immune to fire and necrotic damage.  

Once you use this feature, you can’t do so again until you finish a long rest. 

</blockquote>
</div>

<div class="old-one-patron">
<blockquote>

## Otherworldly Knowledge
At 19th level, your patron has led you to gaze into the black heart of the stars themselves, where all light is eventually consumed. Surviving this experience grants you the ability to assault the psyche of your advesaries with visons from beyond. When you see a creature casting a spell that targets you that requires an Intelligence, Wisdom or Charisma saving throw, you can use your reaction to subject that creature to the untold horrors of the void. The creature must succeed on an Intelligence saving throw against your spell save DC or take psychic damage equal to your Warlock level and you automatically succeed on their saving throw and are unaffected by the spell’s effect.     

</blockquote>
</div>

<div class="shadowfell-patron">
<blockquote>

## Accursed Spectre
At 19th level, you can use a bonus action to magically transform into a form of pure shadow. In this form, you gain the following benefits:  

* You gain a flying (hover) speed equal to your walking speed.  
* You have resistance to all damage except radiant damage.  
* You can move through spaces as narrow as 1 inch wide.  
* You have advantage on Stealth checks.   

This effect ends if you die or if you dismiss it as a bonus action.

</blockquote>
</div>

## Pact Master
At 20th level, you can draw on your inner reserve of mystical power while entreating your patron to fuel your powers. You can spend 1 minute bargaining with your patron to choose expended spell slots to recover. The spell slots can have a combined level equal to your Warlock level.  
Once you use this feature, you can’t do so again until you finish a long rest. 