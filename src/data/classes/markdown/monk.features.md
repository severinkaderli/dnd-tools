## Martial Arts
At 1st level, your practice of martial arts gives you mastery of combat styles that use unarmed strikes. You gain the following benefits while you aren't wearing armor or wielding a shield:

* You can use Dexterity or Strength for the attack and damage rolls of your unarmed strikes.  
* You can roll a d6 in place of the normal damage of your unarmed strike. This die changes to a d8 at 9th level and to a d10 at 17th level.  
* When you use the Attack action on your turn, you can make one unarmed strike as a bonus action.  

## Unarmored Defense
At 1st level, while you are wearing no armor and not wielding a shield, your AC equals 13 + your Wisdom modifier.

## Monk Tradition
At 1st level, you commit yourself to a monk tradition of your choice: the tradition of the open hand, the hidden blade or the immortal soul. Your choice grants you features when you choose it at 1st level and again at 3rd, 7th, 11th, 15th, and 19th level.

<div class="tradition-of-the-open-hand">
<blockquote>

## Way of Devotion
At 1st level, you learn to treat martial arts as an expression of precision, beauty, and intense training. You gain proficiency with the Athletics skill and your choice of either calligrapher's tools or painter's tools.

</blockquote>
</div>

<div class="tradition-of-the-hidden-blade">
<blockquote>

## Shadow Arts
At 1st level, intense martial arts training leads you to master both subterfuge and the use of weapons typical for a hidden blade monk. You gain proficiency with the Stealth skill and martial weapons.
	
</blockquote>
</div>

<div class="tradition-of-the-immortal-soul">
<blockquote>

## Path of Tranquility
At 1st level, you begin your journey to enlightenment. This path follows the believe, that ki is the essence that connects all living things. You gain proficiency in two of the following skills: Animal Handling, Inisght or Persuasion. 
	
</blockquote>
</div>

## Ki
At 2nd level, your training allows you to harness the mystic energy of ki. Your access to this energy is represented by a number of ki points equal to your proficiency bonus + your Wisdom modifier. You can spend these points to fuel various ki features. You regain all expended ki points whenever you finish a short or long rest.  
Some of your ki features require your target to make a saving throw to resist the feature’s effect. The saving throw DC is calculated as follows:

**Ki save DC** = 8 + your proficiency bonus + your Wisdom modifier  

You know the following ki features:

### Deflect Missile
When you are hit by a ranged attack, you can spend 1 ki point and use your reaction to deflect the attack. When you do so, the damage you take from the attack is reduced by 1d10 + your monk level.

### Flurry of Blows
Immediately after you take the Attack action on your turn, you can spend 1 ki point to make two unarmed strikes as a bonus action. 

### Patient Defense
You can spend 1 ki point to take the Dodge action as a bonus action on your turn.

### Step of the Wind
You can spend 1 ki point to take the Dash or Disengage action as a bonus action on your turn, and your jump distance is doubled for the turn. 

### Slow Fall
You can spend 1 ki point and use your reaction when you fall to reduce any falling damage you take by an amount equal to five times your monk level.

## Unarmored Movement
At 2nd level, your speed increases by 10 feet while you are not wearing armor or wielding a shield.  
Additionally, you gain the ability to move along vertical surfaces and across liquids on your turn without falling during the move.

<div class="tradition-of-the-open-hand">
<blockquote>

## Open Hand Technique
At 3rd level, your special martial arts training modifies your ki techniques. Whenever you hit a creature with one of the attacks granted by your Flurry of Blows ki feature, you can impose one of the following effects on that target:

* It must succeed on a Dexterity saving throw or be knocked prone.  

* It must succeed on a Strength saving throw or is pushed up to 15 feet away from you. 

</blockquote>
</div>

<div class="tradition-of-the-hidden-blade">
<blockquote>

## Tools of the Trade
At 3rd level, you learn to create and use the following tools to help you in and out of combat. Some of your tools require your target to make a saving throw. Use your Ki save DC to determine the tool’s effect. You need to have a least one free hand to use such a tool. 

### Fire Crackers
As a bonus action, you can unleash a loud, explosive flash that briefly blinds foes. Each creature within 5 feet of you must succeed on a Dexterity saving throw or is blinded until the end of their next turn. 

### Grappling Claw
The grappling claw allows access to normally unreachable places, acting as a grappling hook. Additionally, you can use your action to launch it at a creature within 20 feet of you. The creature must succeed on a Strength saving throw or you immediately close the gap, moving to an unoccupied space next to it. You can then use your bonus action to make one attack against the target. 

### Smoke Bomb
You can brielfy summon a cloud of smoke that obscures your form. You can take the hide action as a bonus action on your turn. 
	
</blockquote>
</div>

<div class="tradition-of-the-immortal-soul">
<blockquote>

## Astral Training
At 3rd level, your intense training with your ki enables you to project and reuse a portion of it. Your unarmed attacks deal force damage instead of bludgeoning and have the reach property.
Additionally, you regain hit points equal to your Wisdom modifier whenever you spend a ki point.
	
</blockquote>
</div>

## Ability Score Improvement
When you reach 4th level, and again at 6th, 8th, 12th, 14th, and 16th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1, or you can take a feat for which you fulfill the requirements. As normal, you cannot increase an ability score above 20 using this feature.

## Extra Attack
At 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.

<div class="tradition-of-the-open-hand">
<blockquote>

## Ki-Empowered Strikes
At 7th level, you learn to interfere with the flow of ki in an opponent's body. Your unarmed strikes count as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage.  
Additionally, you learn the following ki feature:

### Stunning Strike
When you hit another creature with an unarmed strike, you can spend 1 ki point to attempt to stun it. The target must succeed on a Constitution saving throw or be stunned until the end of it’s next turn.

</blockquote>
</div>

<div class="tradition-of-the-hidden-blade">
<blockquote>

## One with the Blade
At 7th level, you extend your ki into your weapons. Your weapon attacks count as magical for the purpose of overcoming resistance and immunity to nonmagical attacks.  
Additionally, you learn the following ki feature:

### Agile Parry
When you are attacked while wielding a weapon, you can spend 1 ki point and use your reaction to gain a bonus to your AC equal to your Wisdom modifier until the start of your next turn.
	
</blockquote>
</div>

<div class="tradition-of-the-immortal-soul">
<blockquote>

## Immaculate Routine
At 7th level, you have gained the ability to weave offensive and defensive maneuvers in one impressive martial arts display. Whenever you use your Flurry of Blows ki feature, you gain the benefit of the disengage action until the end of your turn.   
Additionally, you learn the following ki feature:

### Focused Offense
Once per turn, when you hit a creature with an unarmed strike, you can spend 1 ki point to heighten your awareness of their attacks. You gain resistance to any damage that creature deals to you until the beginning of your next turn. 
	
</blockquote>
</div>

## Evasion
At 10th level, your instinctive agility lets you dodge out of the way of certain area effects. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.

<div class="tradition-of-the-open-hand">
<blockquote>

## Gentle Touch
At 11th level, you learn to channel a creatures superflous kinetic energy as your own.  When a creature misses you with a melee attack, you can use your reaction to make one unnarmed strike against the creature. If you hit and deal damage, you regain 1 ki point. 

</blockquote>
</div>

<div class="tradition-of-the-hidden-blade">
<blockquote>

## Opportunist
At 11th level, you can exploit a creature’s momentary distraction to fuel your own focus. Once per turn, you can regain 1 ki point if you hit a creature with an attack if you have advantage on the attack roll.
	
</blockquote>
</div>

<div class="tradition-of-the-immortal-soul">
<blockquote>

## Ki Consumption
At 11th level, you gain the ability to harness the energy of passing beings. When a creature within 10 feet of you is reduced to 0 hit points, you can use your reaction to regain 1 ki point.
	
</blockquote>
</div>

## Purity of Body
At 13th level, your mastery of the ki flowing through you grants you proficiency with Constitution saving throws.  
Additionally, it makes you immune to poison and disease. 

<div class="tradition-of-the-open-hand">
<blockquote>

## Forbidden Techniques
At 15th level, learning the forbiden techniques of the open hand tradition has granted you the ability to use the following ki feature:

### Quivering Palm
When you hit a creature with three or more unarmed strikes on your turn, you can spend 3 ki points to set up lethal vibration’s in it’s body. When you do so, the target must make a Constitution saving throw. If it fails, it is reduced to 0 hit points. If it suceeeds, it takes 10d10 necrotic damage.

</blockquote>
</div>

<div class="tradition-of-the-hidden-blade">
<blockquote>

## Secret Scroll
At 15th level, reading the secret scroll of the hidden blade tradition has granted you the ability to use the following ki feature:

### Sharpen the Blade
As a bonus action, you can expend up to 3 ki points to grant one weapon you are wielding a bonus to attack and damage rolls equal to the number of ki points you spent. The bonus lasts for 1 minute or until you use this feature again. This feature has no effect on a magic weapon that already has a bonus to attack and damage rolls.
	
</blockquote>
</div>

<div class="tradition-of-the-immortal-soul">
<blockquote>

## Sacred Utterances
At 15th level, learning the sacred utterances of the immortal soul tradition has granted you the ability to use the following ki feature:

### Touch of Disruption
As a bonus action on your turn, you can charge a devastating attack that threatens to disrupt a creature’s ki. The next time you hit a creature with an unarmed strike this turn, you can spend 3 ki points to deal 5d10 additional force damage. For one minute, a creature hit by this attack can’t regain lost hit points, has vulnerability to force damage and cannot take reactions. 
	
</blockquote>
</div>

## Purity of Mind
At 18th level, you learn to focus your ki in your mind, protecting and reinforcing it. You gain proficiency with Wisdom saving throws.  
Additonally, you are immune against being charmed or frightened and have resistance to psychic damage.

<div class="tradition-of-the-open-hand">
<blockquote>

## Master of the Open Hand
At 19th level, your mastery of unnarmed strikes has perfected your martial arts, granting you nearly unreachable consistency. You no longer need to spend ki points to you use your Flurry of Blows ki feature. 

</blockquote>
</div>

<div class="tradition-of-the-hidden-blade">
<blockquote>

## Master of the Hidden Blade
At 19th level, your mastery of weapons grants you extraordinary accuracy. If you miss with a weapon attack on your turn, you can reroll it. You can use this feature only once on each of your turns.
	
</blockquote>
</div>

<div class="tradition-of-the-immortal-soul">
<blockquote>

## Master of the Immortal Soul
At 19th level, your mastery of your exceptionally powerful ki allows you to summon a translucent embodiment of your psyche, soul and ki. As a bonus action on your turn, you can enter a deep meditative state, which summons four arms, which hover above your shoulder, a visage that masks your face and an armor that covers your body. For 10 minutes, or until you use a bonus action to end the effect prematurely, you gain the following benefits:

* The arms grant you one additional attack whenever you take the Attack action on your turn.   
* The visage grants you truesight up to a range of 120 feet.   
* The armor grants you a +2 bonus to your AC.   

Once you use this feature, you can’t use it again until you finish a short or long rest. 
	
</blockquote>
</div>

## Transcendent Master
At 20th level, you have reached the pinnacle of monastic perfection. Your body and mind are flawless constructs of your willpower and aspiration. Your ki sustains you so that you suffer none of the frailty of old age. For every 10 years that pass, your body ages only 1 year. You can’t be aged magically and you no longer need food or water.  
Additionally, you learn the following ki feature:

### Perfect Soul
Whenever you fail a saving throw, you can spend 1 ki point to reroll it and take the second result. 
