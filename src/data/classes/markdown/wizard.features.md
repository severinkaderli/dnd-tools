## Spellcasting
At 1st level, as a student of arcane magic, you are able to cast spells at will.

### Spells known
You know three 1st-level spells of your choice from the Wizard spell list. The Spells Known column of the Wizard table shows when you learn more spells of your choice. Each of these spells must be of a level for which you have spell slots, as shown on the table. For instance, when you reach 3rd level in this class, you can learn new spells of 1st or 2nd level.

### Cantrips
At 1st level, you know two cantrips of your choice from the Wizard spell list. You learn additional Wizard cantrips of your choice at higher levels, as shown in the cantrips column of the Wizard table.

### Spellcasting Ability
Intelligence is your spellcasting ability for your Wizard spells. In addition, you use your Intelligence modifier when setting the saving throw DC for a Wizard spell you cast and when making an attack roll with one.

**Spell save DC:** 8 + your proficiency bonus + your Intelligence modifier  
**Spell attack:** your proficiency bonus + your Intelligence modifier

### Spellcasting Focus
You can use your spellbook for casting your Wizard spells.

## Spellbook
You have a spellbook which contains your Wizard spells. Whenever you learn a new Wizard spell as part of a gaining a level in this class, the spell is automatically added to the spellbook.  
Additionally, when you find a wizard spell of 1st level or higher in form of a scroll, an inscription or another wizard’s spellbook, you can add it to your spellbook if it is of a level for which you have spell slots and if you can spare the time to decipher and copy it. For each level of the spell, the process takes 2 hours and costs 50 gp. The cost represents material components you expend as you experiment with the spell to master it. Once you have spent this time and money, you add the spell to your known spells and you can cast the spell just like your other spells.  
If you lose your spellbook, you lose access to all your Wizard spells. For this reason, many wizards keep backup spellbooks in a safe place. You can use the same procedure to transcribe the spells that you know into a new spellbook. 

## Wizard Tradition
You choose an arcane tradition, shaping your practice of magic. Your choice grants you features when you choose it at 1st level and again at 2nd, 6th, 10th, 14th, and 18th level.

<div class="tradition-of-the-scholar">
<blockquote>

## Student of Arcanology
At 1st level, you pursue a tradition fixated on understanding the underlying mechanics of magic. Your studies lead to the uncovering of new knowledge. The time and money that you must spend to copy a spell into your spellbook is halved.  
You also gain access to the following tradition spells:

### Tradition Spells
You gain access to certain spells connected to your studies, called tradition spells. These spells always count as Wizard spells for you. Once you gain access to a tradition spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Pact Spells |
|-----|---------------------|
| 1st | _[Chronal Lance](spells#chronal-lance)_ |
| 2nd | _[Soul Shield](spells#soul-shield)_ |
| 3rd | _[Twisted Magic](spells#zwisted-magic)_ |
| 4th | _[Flickering Fate](spells#flickering-fate)_ |
| 5th | _[Lay Field](spells#ley-field)_ |

</blockquote>
</div>

<div class="tradition-of-the-war-mage">
<blockquote>

## Student of Conflict
At 1st level, your studies blend principles of evocation and abjuration, creating techniques that empower your spells, while also providing methods to bolster your defenses. Whenever you finish a long rest, you can choose one 1st-level or higher evocation or abjuration spell in your spellbook and replace it with another evocation or abjuration spell of the same level from the wizard spell list.  
You also gain access to the following tradition spells:

### Tradition Spells
You gain access to certain spells connected to your studies, called tradition spells. These spells always count as Wizard spells for you. Once you gain access to a tradition spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Pact Spells |
|-----|---------------------|
| 1st | _[Impressive Blow](spells#impressive-blow)_ |
| 2nd | _[Charged Missile](spells#charged-missile)_ |
| 3rd | _[Battle Double](spells#battle-double)_ |
| 4th | _[Spectral Archers](spells#spectral-archers)_ |
| 5th | _[Rain of Blades](spells#rain-of-blades)_ |

</blockquote>
</div>

<div class="tradition-of-the-necromancer">
<blockquote>

## Student of Undeath
At 1st level, your studies alter your existence to a state between life and death. You always count as an undead for all spells and abilities that affect them. You do no longer need air, nourishment, water or light to live, but you can still ingest food and drink if you wish. You gain resistance to necrotic damage.  
You also gain access to the following tradition spells:

### Tradition Spells
You gain access to certain spells connected to your studies, called tradition spells. These spells always count as Wizard spells for you. Once you gain access to a tradition spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Pact Spells |
|-----|---------------------|
| 1st | _[Debilitate](spells#debilitate)_ |
| 2nd | _[Pain of Giving](spells#pain-of-giving)_ |
| 3rd | _[Life Transference](spells#life-transference)_ |
| 4th | _[Creeping Eye](spells#creeping-eye)_ |
| 5th | _[Skull Bomb](spells#skull-bomb)_ |

</blockquote>
</div>

## Wizard Familiar
At 2nd level, you gain the service of a familiar, an artificially created spirit that takes the form of a small or tiny beast of your choice. The familiar has the statistics of the chosen form, though it is a celestial, fey, or infernal (your choice) instead of a beast. Your familiar acts independently of you, but it always obeys your commands. In combat, it rolls its own initiative and acts on its own turn.  
When the familiar drops to 0 hit points, it disappears, leaving behind no physical form. It reappears beside you after you finish a long rest. You can telepathically communicate with your familiar aslong it remains within 100 feet of you. Additionally, as an action, you can see through your familiar's eyes and hear what it hears, as long as you concentrate on it. During this time, you are deaf and blind with regard to your own senses.   
As a bonus action, you can temporarily dismiss your familiar. It disappears into a pocket dimension where it awaits your summons. As a bonus action while it is temporarily dismissed, you can cause it to reappear in any unoccupied space which you can see within 30 feet of you. 
You can command it to adopt a new form over the course of a short or long rest. Choose a small or tiny beast. Your familiar transforms into the chosen creature.  
Finally, when you cast a spell with a range of touch, your familiar can deliver the spell as if it had cast the spell. Your familiar must be within 100 feet of you, and it must use its reaction to deliver the spell when you cast it. If the spell requires an attack roll, you use your attack modifier for the roll.  

<div class="tradition-of-the-scholar">
<blockquote>

## Wordly Secrets
At 2nd level, you branch your studies to another source of magic. Select one of the following options:

* **Nature**  
Whenever you gain a wizard level, you can replace one of the wizard spells you add to your spellbook with any spell from the Druid spell list instead. The spell must still be of a level for which you have spell slots.

* **Religion**  
Whenever you gain a wizard level, you can replace one of the wizard spells you add to your spellbook with any spell from the Cleric spell list instead. The spell must still be of a level for which you have spell slots.

Any spells you gain from this feature is considerer a Wizard spell for you, but other Wizards can’t copy these spells from your spellbook. 

</blockquote>
</div>

<div class="tradition-of-the-war-mage">
<blockquote>

## Sculpt Spells
At 2nd level, you can create pockets of relative safety within the effects of your spells. When you cast a spell that affects multiple creatures that you can see, you can choose a number of creatures who then automatically succeed on their saving throws against the spell and they take no damage from it and are otherwhise unaffected by it.

</blockquote>
</div>

<div class="tradition-of-the-necromancer">
<blockquote>

## Undead Thralls
At 2nd level, you unlock the knowledge to create undead servants. As an action, you or your familiar can touch a corpse of a humanoid to imbue the matter with false life and raise it as an Undead Thrall under your control (The DM will provide fitting statistics).
You can controll a number of Thralls equal to your Intelligence modifier. Should you ever gain control over an Undead Thrall and you exceed this number, the one longest under your control is immediately destroyed. 
Your Undead Thralls roll for initiative as a group. You determine their actions, decisions and attitudes. They do not act on their own or without direction. If you are incapacitated or absent, your Undead Thralls follow the latest command that was given to them by you or otherwise do nothing. 
Your Undead Thralls have abilities and game statistics determined by you. They use your proficiency bonus rather than their own. An Undead Thrall also adds your Intelligence modifier to its AC and to its damage rolls.
If one of your Undead Thralls is killed, the magic within it consumes and destroys the body and it cannot be resurrected through means that require a body.

</blockquote>
</div>

## Arcane Restoration
At 3rd level, you have learned to regain some of your magical energy by studying your spellbook. Once per day when you finish a short rest, you can choose expended spell slots to recover. The spell slots can have a combined level that is equal to or less than half your wizard level (rounded down).

## Ability Score Improvement
When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1, or you can take a feat for which you fulfill the requirements. As normal, you cannot increase an ability score above 20 using this feature.

<div class="tradition-of-the-scholar">
<blockquote>

## Improved Concentration
At 6th level, our focus on spells becomes adamant. Whenever you would lose concentration on a spell, you can instead choose to maintain focus by expending a spell slot of an equal or greater level than the spell you would lose concentration on.

</blockquote>
</div>

<div class="tradition-of-the-war-mage">
<blockquote>

## Arcane Ward
At 6th level, you learn to weave magic around yourself for protection. You create a magical ward on yourself. Whenever you cast a wizard spell of 1st level or higher, the ward gains a number of hit points equal to the spell’s level + your Intelligence modifier. Whenever you take damage, the ward takes the damage instead. If this damage reduced the ward to 0 hit points, you take any remaining damage. The ward has a maximum amount of hit points equal to twice your wizard level.

</blockquote>
</div>

<div class="tradition-of-the-necromancer">
<blockquote>

## Improved Thrall
At 6th level, your Undead Thralls become more resilient to damage and are harder to kill. They gain the following benefits:

* Their AC cannot be less than 14.  
* Their maximum hit points are increased by an amount  equal to your Wizard level.  
* They gain resistance to bludgeoning, piercing and slashing damage from non-magical sources.

</blockquote>
</div>

## Signature Spells
At 10th level, you have achieved such mastery over certain spells that you can cast them at will. Choose a 1st-level wizard spell and a 2nd-level wizard spell that are in your spellbook. You can cast those spells at their lowest level without expending a spell slot and without dependency to your spellbook. If you want to cast either spell at a higher level, you must expend a spell slot as normal.

<div class="tradition-of-the-scholar">
<blockquote>

## Lore Master
At 10th level, you study has led you to become a compendium of knowledge and understanding. Whenever you make an Intelligence check that lets you add your proficiency bonus, you can treat a d20 roll of 9 or lower as a 10.

</blockquote>
</div>

<div class="tradition-of-the-war-mage">
<blockquote>

## Siege spells
At 10th level, you learn the long-range, destructive magic iconic for a war mage. You double the range of your spells, and then increase the range of spells with a range of at least 100 feet to 1 mile.  
Additionally, your spells ignore half and three-quarters cover.  

</blockquote>
</div>

<div class="tradition-of-the-necromancer">
<blockquote>

## Command Undead
At 10th level, At 10th level, you can use magic to bring undead that were created by other forces under your control. As an action, you can choose one undead that you can see within 60 feet of you. That creature must make a Charisma saving throw against your Wizard spell save DC. If it succeeds, you cannot use this feature on the same undead creature ever again. If it fails the save, it permanently becomes one of your Undead Thralls. These Thralls count against your limit of Thralls that you can control. 
Intelligent undead are harder to control in this way. If the target has an Intelligence of 8 or higher, it has advantage on the saving throw. If it fails the saving throw and has an Intelligence of 14 or higher, it can repeat the saving throw at the end of every hour until it succeeds and breaks free.
You cannot use this feature on undead with an Intelligence of 18 or higher.
 
</blockquote>
</div>

<div class="tradition-of-the-scholar">
<blockquote>

## Overchannel
At 14th level, you learn to maximize the power of your simpler spells. When you cast a wizard spell of 1st through 3rd-level, you can choose to either have it deal maximum damage or to force the target to suffer disadvantage against the saving throw of the spell. 
Whenever you use this feature, you suffer one level of exhaustion. 

</blockquote>
</div>

<div class="tradition-of-the-war-mage">
<blockquote>

## Improved Arcane Ward
At 14th level, the potency of your ward increases. While under the effect of your Arcane Ward feature, and the ward has more than 0 hit points, you gain a +2 bonus to AC and gain resistance to all damage except psychic damage. 

</blockquote>
</div>

<div class="tradition-of-the-necromancer">
<blockquote>

## Emissary of Death
At 14th level, your might grows as the dark magic within you allows for even greater feats. Whenver you deal damage with a necromancy spell, you can add you Intelligence modifier to the damage roll. When you do so, you regain 1d10 hit points. 

</blockquote>
</div>

<div class="tradition-of-the-scholar">
<blockquote>

## Master of Magic
At 18th level, your knowledge of magic allows you to duplicate almost any spell. As an action, you can call to mind the ability to cast one spell of your choice from any class’s spell list. The spell must be of a level for which you have spell slots and you follow the normal rules for casting it, including expending a spell slot. If the spell is not a wizard spell, it counts as a wizard spell when you cast it. The ability to cast the spell vanishes from your mind when you cast it or when you finish your next long rest.  
You cannot use this feature again until you finish a long rest.

</blockquote>
</div>

<div class="tradition-of-the-war-mage">
<blockquote>

## Piercing Magic
At 18th level, as a seasoned veteran of might and magic, you tune your spells for maximum efficiency. Creatures with the Magic Resistance trait no longer have advantage on saving throws against wizard spells that you cast.
Additionally, damage done with wizard spells cast by you ignore resistances.

</blockquote>
</div>

<div class="tradition-of-the-necromancer">
<blockquote>

## Indestructible Life
At 18th level, you partake some of the true secrets of undeath. 
At the start of your turn, if you have no more than half of your hit points left, you can choose to regain hit points equal to twice your wizard level. Each undead thrall under your controll that you can see regains the same amount. Once you use this feature, you must finish a long rest before you can use it again.

</blockquote>
</div>

## Magus
At 20th level, you can choose a 3rd-level wizard spell that is in your spellbook and you learn to cast it at its lowest level without expending a spell slot and without dependency to your spellbook.
