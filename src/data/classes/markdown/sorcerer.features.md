## Spellcasting
At 1st level, an event in your past, or in the life of a parent or ancestor, left an indelible mark on you, infusing you with arcane magic. This font of magic, whatever its origin, fuels your spells.

### Spells known
You know three 1st-level spells of your choice from the Sorcerer spell list. The Spells Known column of the Sorcerer table shows when you learn more spells of your choice. Each of these spells must be of a level for which you have spell slots, as shown on the table. For instance, when you reach 3rd level in this class, you can learn new spells of 1st or 2nd level.

### Cantrips
At 1st level, you know two cantrips of your choice from the Sorcerer spell list. You learn additional Sorcerer cantrips of your choice at higher levels, as shown in the Cantrips column of the Sorcerer table.

### Spellcasting Ability
Charisma is your spellcasting ability for your Sorcerer spells. In addition, you use your Charisma modifier when setting the saving throw DC for a Sorcerer spell you cast and when making an attack roll with one.

**Spell save DC:** 8 + your proficiency bonus + your Charisma modifier    
**Spell attack:** your proficiency bonus + your Charisma modifier   

## Living Focus
At 1st level, due to your innate magical abilities, your body is able to channel arcane powers through itself. You can ignore the material component of Sorcerer spells that you cast.  

## Sorcerous Bloodline
At 1st level, choose an arcane bloodline, which describes the source of your innate magical power. Your choice grants you features when you choose it at 1st level and again at 2nd, 6th, 10th, 14th, and 18th level.

<div class="draconic-bloodline">
<blockquote>

## Dragon Ancestor
At 1st level, your bloodline, draconic magic that was mingled with your blood or that of your ancestors, grants you an innate connection to draconic creatures. You can speak, read, and write Draconic.     
You also gain access to the following bloodline spells:

### Bloodline Spells
You gain access to certain spells connected to your source of magic, called bloodline spells. These spells always count as Sorcerer spells for you. Once you gain access to a bloodline spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Bloodline Spells |
|-----|---------------------|
| 1st | _[Enhance Greed](spells#enhance-greed)_ |
| 2nd | _[Draconic Roar](spells#draconic-roar)_ |
| 3rd | _[Ancient Shield](spells#ancient-shield)_ |
| 4th | _[Scale Rot](spells#scale-rot)_ |
| 5th | _[Dragonshift](spells#dragonshift)_ |

</blockquote>
</div>

<div class="phoenix-bloodline">
<blockquote>

## Phoenix Ancestor
At 1st level, a shard of the phoenix's power dwells within you. You gain the ability to start fires with a touch. As an action, you can magically ignite a flammable object you touch with your hand.    
You also gain access to the following bloodline spells:

### Bloodline Spells
You gain access to certain spells connected to your source of magic, called bloodline spells. These spells always count as Sorcerer spells for you. Once you gain access to a bloodline spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Bloodline Spells |
|-----|---------------------|
| 1st | _[Molten Chains](spells#molten-chains)_ |
| 2nd | _[Warmth](spells#warmth)_ |
| 3rd | _[Flame Wave](spells#flame-wave)_ |
| 4th | _[Reverberate](spells#reverberate)_ |
| 5th | _[From Ashes](spells#from-ashes)_ |

</blockquote>
</div>

<div class="vampiric-bloodline">
<blockquote>

## Vampire Ancestor
At 1st level, your accursed blood gifts you with unholy powers. You can see in darkness as if it were dim light.     
You also gain access to the following bloodline spells:

### Bloodline Spells
You gain access to certain spells connected to your source of magic, called bloodline spells. These spells always count as Sorcerer spells for you. Once you gain access to a bloodline spell, you add it to your list of spells known and it does not count against the number of spells you can learn. 

| Spell Level | Bloodline Spells |
|-----|---------------------|
| 1st | _[Hypnotic Gaze](spells#hyptnotic-gaze)_ |
| 2nd | _[Blood Rebuke](spells#blood-rebuke)_ |
| 3rd | _[Visage of Madness](spells#visage-of-madness)_ |
| 4th | _[Exsanguinating Cload](spells#exsanguinating-cloud)_ |
| 5th | _[Risen Road](spells#risen-road)_ |

</blockquote>
</div>

## Metamagic
At 2nd level, you tap into a deep wellspring of magic within yourself. This wellspring is represented by sorcery points equal to your proficiency bonus + your Charisma modifier. You can spend these points to create a varierty of magical effets. You regain all expended sorcery points whenever you finish a long rest. You learn two of the following metamagic options of your choice. You learn two additional metamagic options once you reach 10th level in this class. 

<details>
<summary>Metamagic Options</summary>

### Careful Spell
When you cast a spell that affects multiple creatures that you can see, you can spend 1 sorcery point and choose a number of creatures who then automatically succeed on their saving throw against the spell and they take no damage from it and are otherwhise unaffected by it.

### Disruptive Spell
When you cast a spell and deal damage with it, you can spend 1 sorcery point to give targets of that spell disadvantage on concentration checks until the end of your turn. 

### Distant Spell
When you cast a spell, you can spend 1 sorcery point to double the range of the spell. If you use this option on a spell that has a range of touch, you increase the range of that spell to 30 feet.

### Empowered Spell
When you cast a spell and deal damage with it, you can spend 1 sorcery point to reroll any of that spell’s damage dice. You must use the new rolls.

### Heightened Spell
When you cast a spell that forces a creature to make a saving throw to resist its effects, you can spend 2 sorcery points to give one target of the spell disadvantage on its first saving throw it makes against the spell.

### Quickened Spell
When you cast a spell that has a casting time of 1 action, you can spend 2 sorcery points to change the casting time to 1 bonus action for this casting.

### Subtle Spell
When you cast a spell, you can spend 1 sorcery point to cast it without any somatic or verbal components.

### Twinned Spell
When you cast a spell that targets only one creature, you can spend 2 sorcery points to target a second creature in range with the same spell. 

### Unerring Spell
When you cast a spell that requires you to make a spell attack roll and you miss, you can spend 2 sorcery points to reroll the attack roll. You must use the result of the second roll.

</details>

<div class="draconic-bloodline">
<blockquote>

## Scaleborn
At 2nd level, the magic flowing through your body causes physical traits of your dragon ancestors to emerge. Your hit point maximum increases by 2 and increases by 1 again whenever you gain a level in this class.  
Additionally, parts of your skin are covered by a thin sheen of dragon-like scales. When you aren't wearing armor, your AC equals 13 + your Charisma modifier.

</blockquote>
</div>

<div class="phoenix-bloodline">
<blockquote>

## Mantle of Flame
At 2nd level, you can unleash the phoenix fire that blazes within you. You can use your bonus action and expend one spell slot to magically wreath yourself in swirling fire, as your eyes glow like hot coals. For 1 minute, you gain the following benefits:

* You shed bright light in a 30-foot radius and dim light for an additional 30 feet.  
* Creatures that touch you, hit you with a melee attack, or start their turn within 5 feet of you, take 1 fire damage per level of the spell slot you expend.  
* Whenever you roll fire damage on your turn, you deal 1 additonal fire damage per level of the spell slot you expend.  

</blockquote>
</div>

<div class="vampiric-bloodline">
<blockquote>

## Unholy Form
At 2nd level, due to your vampiric heritage, your body changes and shows signs of it’s corruption. You gain resistance to necrotic damage.
Additionally, you grow a set of fangs, which you can use to make unarmed strikes. They deal 1d6 piercing damage and you can use Dexterity instead of Strength for the attack and damage rolls. If you hit with it, you can choose to spend one hit die to regain hit points equal to the number rolled + the damage dealt with your fangs. Double this amount if you hit a creature that was charmed by you.

</blockquote>
</div>

## Magical Burn
At 3rd level, you learn to overexert yourself to channel more power than normal, pushing past the limit of what is safe for your body. At the start of your turn, if you have no sorcery points left, you can choose to spend one hit die and take necrotic damage equal to the number rolled to regain one sorcery point. This damage ignores resistance and immunity and can’t be prevented.

## Ability Score Improvement
When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one Ability Score of your choice by 2, or you can increase two Ability Scores of your choice by 1, or you can take a feat for which you fulfill the requirements. As normal, you cannot increase an Ability Score above 20 using this feature.

<div class="draconic-bloodline">
<blockquote>

## Ancient Knowledge
At 6th level, you gain the ability to magically sense the presence of your ancestors. You can use your action and expend one spell slot to focus your awareness on the region around you. For 1 minute, you can sense the presence and vague location of any dragon creature within 1 mile per level of the spell slot you expend.  
You also learn the following metamagic option:

### Frightening Spell
When you cast a spell and deal damage with it, you can spend 1 sorcery point to make one target of your spell become frightened by you. 

</blockquote>
</div>

<div class="phoenix-bloodline">
<blockquote>

## Heart of Fire
At 6th level, he fiery energy within you grows stronger. You gain resistance to fire damage.   
You also learn the following metamagic option:
 
### Burning Spell
When you cast a spell and deal fire damage with it, you can spend 2 sorcery points to set one target of your spell ablaze. The afflicted creature takes fire damage equal to half the damage taken by that spell at the end of its next turn. The creature can use its action on its turn to quench the flame and take no damage instead. 

</blockquote>
</div>

<div class="vampiric-bloodline">
<blockquote>

## Dark Beckoning
At 6th level, you learn to punish those that strain against your magic with volatile energy. When a creature that you can see succeeds on a saving throw against a Sorcerer spell cast by you,
you can choose to expend one spell slot to deal necrotic damage to that creature equal to your Charisma modifier + 1 additonal necrotic damage per level of the spell slot you expend.   
You also learn the following metamagic option: 

###	Beguiling Spell
When you cast an enchantment spell and the target fails its saving throw you can spend 1 sorcery point to make one target of that spell unaware of your magical influence on it. You alter it’s understanding so that it remains unaware of the spell being cast and it’s effect, even after the spell fades.

</blockquote>
</div>

<div class="draconic-bloodline">
<blockquote>

## Dragon Wings
At 10th level, you gain the ability to sprout a pair of dragon wings from your back. As a bonus action on your turn, you can create these wings and gain a flying speed of 50 feet. To use this speed, you cannot be wearing medium or heavy armor. They last until you dismiss them as a bonus action on your turn.

</blockquote>
</div>

<div class="phoenix-bloodline">
<blockquote>

## Phoenix Spark
At 10th level, you learn to controll your innate magic to preserve you in the face of defeat. If you are reduced to 0 hit points while you are under the effect of your Mantle of Flame feature, you can use your reaction to draw on the spark of the phoenix. You are instead reduced to 1 hit point, and each creature within 10 feet of you takes fire damage equal to your Sorcerer level + 1 fire damage per level of the spell slot you expended to activate Mantle of Flame. Your Mantle of Flame then immediately ends.

</blockquote>
</div>

<div class="vampiric-bloodline">
<blockquote>

## Mantle of Whispers
At 10th level, you gain the ability to adopt a humanoid’s persona. When a humanoid that you can see fails a saving throw against a Sorcerer spell cast by you, you can choose to magically capture it’s essence. You retain this essence until you until you use it. You can spend the essence as an action. When you do so, it vanishes, magically transforming into a disguise that changes your appearance to be that of the creature. This is an illusory effect and your statistics remain the same. You have advantage on Charisma checks made to pass yourself off as that humanoid. This disguise lasts for 24 hours or until you end it as a bonus action.

</blockquote>
</div>

<div class="draconic-bloodline">
<blockquote>

## Magic Resistance
At 14th level, your ancient magic lies so heavily upon you that it forms a protective ward. You have resistance to damage from spells. 

</blockquote>
</div>

<div class="phoenix-bloodline">
<blockquote>

## Soul of Fire
At 14th level, your innate magic strengthens your fire. Fire damage dealt by Sorcerer spells you cast or by features gained by this bloodline ignore resistance to fire damage. 

</blockquote>
</div>

<div class="vampiric-bloodline">
<blockquote>

## Dark Empowerment
At 14th level, you can draw your blood into arcane might. As a bonus action on your turn, you can choose to lose a number of hit points to increase your spell’s effectiveness. You gain a +1 bonus to your spell attack rolls and spell save DC for every 10 hit points sacrificed, up to a maximum of +3. Your hit point maximum is reduced by the same amount. The bonus and the hit point reduction remain until you finish a long rest.

</blockquote>
</div>

<div class="draconic-bloodline">
<blockquote>

## Commanding Presence
At 18th level, your draconic essence infuses your appearance with imposing grandeur. Whenever you make a Charisma check, you can treat a d20 roll of 9 or lower as a 10.

</blockquote>
</div>

<div class="phoenix-bloodline">
<blockquote>

## Form of the Pheonix
At 18th level, you finally master the spark of fire that dances within you. While under the effect of your Mantle of Flame feature, you gain additional benefits:

*	You summon a pair of fiery wings and gain a flying speed of 50 feet. To use this speed, you cannot be wearing medium or heavy armor.  
*	You gain immunity to fire damage.  
*   Damage dealt with your Mantle of Flame feature and your Pheonix Spark feature is doubled.  

</blockquote>
</div>

<div class="vampiric-bloodline">
<blockquote>

## Crimson Lord
At 18th level, as your blood manifests its true potential, you reache the pinnacle of your deathly power. You gain the following benefits:

* You gain immunity against diseases and both the poisoned and paralyzed condition.  
* You gain a hover speed equal to your walking speed.  
* Creatures with a CR lower than your level have disadvantage on saving throws against Sorcerer spells cast by you.  

</blockquote>
</div>

## Master of Magic
At 20th level, you learn to transcend normal spellcasting and start bending all creation to your will. You reduce the sorcery point cost of your known metamagic options by one. 