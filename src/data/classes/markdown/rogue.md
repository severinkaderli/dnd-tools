## Hit Points
**Hit Dice:** 1d8 per Rogue level  
**Hit Points at 1st level:** 8 + your Constitution modifier  
**Hit Points at higher levels:** 1d8 (or 4) + your Constitution modifier per Rogue level after the 1st  

## Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Dexterity and Intelligence  
**Armor:** light armor  
**Weapons:** simple weapons, martial weapons  
**Tools:** thieve's tools  
**Languages:** Thieve's Cant and one language of your choice  
**Skills:** Stealth and three skills of your choice   

## Starting Equipment
You start with the following items, plus anything provided by your background:

* two martial melee weapon 
* a simple ranged weapon 
* leather armor and thieve's tools 
* a burglar's pack or a dungeoneer's pack 