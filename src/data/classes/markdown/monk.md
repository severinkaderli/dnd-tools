## Hit Points
**Hit Dice:** 1d10 per Monk level  
**Hit Points at 1st level:** 10 + your Constitution modifier  
**Hit Points at higher levels:** 1d10 (or 5) + your Constitution modifier per Monk level after the 1st

## Starting Proficiencies
You are proficient with the following items, in addition to any proficiencies provided by your race or background:

**Saving Throw:** Strength and Dexterity  
**Armor:** none  
**Weapons:** simple weapons  
**Tools:** one tool of your choice  
**Languages:** one language of your choice  
**Skills:** three skills of your choice

## Starting Equipment
You start with the following items, plus anything provided by your background:

* one simple weapon or one martial weapon (if proficient)
* a dungeoneer’s pack or an explorer’s pack
