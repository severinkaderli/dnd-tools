Sorcerers carry a magical birthright conferred upon them by an exotic bloodline. One can't study sorcery as one learns a language, any more than one can learn to live a legendary life. No one chooses sorcery; the power chooses the sorcerer. 

Magic is a part of every sorcerer, suffusing body, mind, and spirit with a latent power that waits to be tapped. The appearance of sorcerous powers is wildly unpredictable. Some bloodlines produce exactly one sorcerer in every generation, but in other lines of descent every individual is a sorcerer. Most of the time, the talents of sorcery appear as apparent flukes. Some sorcerers can't name the origin of their power, while others trace it to strange events in their own lives.

Sorcerers have no use for the spellbooks and ancient tomes, nor do they rely on a patron or a diety. By learning to harness and channel their own inborn magic, they can discover new and staggering ways to unleash that power. People with magical power seething in their veins soon discover that the power doesn't like to stay quiet. A sorcerer's magic wants to be wielded. 

The most important question to consider when creating your sorcerer is the origin of your power. Is it a family curse, passed down to you from distant ancestors? Or did some extraordinary event leave you blessed with inherent magic but perhaps scarred as well?

_"Practice and study are for amateurs. True power is a birthright."_

## Gallery
<div class="gallery">

![Sorcerer 1](img/classes/sorcerer_1.jpg)
![Sorcerer 2](img/classes/sorcerer_2.jpeg)
![Sorcerer 3](img/classes/sorcerer_3.jpeg)
![Sorcerer 4](img/classes/sorcerer_4.jpg)
![Sorcerer 5](img/classes/sorcerer_5.png)
![Sorcerer 6](img/classes/sorcerer_6.jpeg)

</div>
