import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  linkExactActiveClass: "is-active",
  routes: [
    {
      path: "/",
      redirect: "/races",
    },
    {
      path: "/classes",
      component: () =>
        import(/* webpackChunkName: "classes" */ "./views/Classes.vue"),
    },
    {
      path: "/races",
      component: () =>
        import(/* webpackChunkName: "races" */ "./views/Races.vue"),
    },
    {
      path: "/spells",
      component: () =>
        import(/* webpackChunkName: "spells" */ "./views/Spells.vue"),
    },
    {
      path: "/feats",
      component: () =>
        import(/* webpackChunkName: "feats" */ "./views/Feats.vue"),
    },
    {
      path: "/equipment",
      component: () =>
        import(/* webpackChunkName: "equipment" */ "./views/Equipment.vue"),
    },
    {
      path: "/items",
      component: () =>
        import(/* webpackChunkName: "items" */ "./views/Items.vue"),
    },
  ],
});
